﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainConsole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainConsole))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.txtVersion = New System.Windows.Forms.TextBox()
        Me.VersionToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnHYC = New System.Windows.Forms.Button()
        Me.btnKillProcesses = New System.Windows.Forms.Button()
        Me.btnSendPrelimPricing = New System.Windows.Forms.Button()
        Me.btnCreateMissingPNFiles = New System.Windows.Forms.Button()
        Me.btnBug = New System.Windows.Forms.Button()
        Me.btnFLValidation = New System.Windows.Forms.Button()
        Me.btnValFax = New System.Windows.Forms.Button()
        Me.btnJustLooking = New System.Windows.Forms.Button()
        Me.btnSS = New System.Windows.Forms.Button()
        Me.btnValidate = New System.Windows.Forms.Button()
        Me.btnReturnPW = New System.Windows.Forms.Button()
        Me.btnCompanyPW = New System.Windows.Forms.Button()
        Me.btnMain = New System.Windows.Forms.Button()
        Me.btnBuildVRFile = New System.Windows.Forms.Button()
        Me.btnReceiptAck = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblItemsRemaining = New System.Windows.Forms.Label()
        Me.lblBlueFlags = New System.Windows.Forms.Label()
        Me.lblRedFlags = New System.Windows.Forms.Label()
        Me.lblLastCalcDate = New System.Windows.Forms.Label()
        Me.lblLastUpload = New System.Windows.Forms.Label()
        Me.lblLastFileSave = New System.Windows.Forms.Label()
        Me.lblValidationStatus = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblCombo = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.lblCompany = New System.Windows.Forms.Label()
        Me.btnFuelLube = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboConsultant = New System.Windows.Forms.ComboBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cboRefNum = New System.Windows.Forms.ComboBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.cboStudy = New System.Windows.Forms.ComboBox()
        Me.Help = New System.Windows.Forms.HelpProvider()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.tabQA = New System.Windows.Forms.TabPage()
        Me.tv = New System.Windows.Forms.TreeView()
        Me.chkUseRefnum = New System.Windows.Forms.CheckBox()
        Me.QASearch = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lnkResultsPresentation = New System.Windows.Forms.LinkLabel()
        Me.lnkPricing = New System.Windows.Forms.LinkLabel()
        Me.lnkMisc = New System.Windows.Forms.LinkLabel()
        Me.lnkProcessData = New System.Windows.Forms.LinkLabel()
        Me.lnkOpex = New System.Windows.Forms.LinkLabel()
        Me.lnkMaintenance = New System.Windows.Forms.LinkLabel()
        Me.lnkPersonnel = New System.Windows.Forms.LinkLabel()
        Me.lnkEnergy = New System.Windows.Forms.LinkLabel()
        Me.lnkMaterialBalance = New System.Windows.Forms.LinkLabel()
        Me.lnkStudyBoundary = New System.Windows.Forms.LinkLabel()
        Me.lnkRefineryHistory = New System.Windows.Forms.LinkLabel()
        Me.tabClippy = New System.Windows.Forms.TabPage()
        Me.btnQueryQuit = New System.Windows.Forms.Button()
        Me.lblError = New System.Windows.Forms.Label()
        Me.chkSQL = New System.Windows.Forms.CheckBox()
        Me.dgClippyResults = New System.Windows.Forms.DataGridView()
        Me.btnClippySearch = New System.Windows.Forms.Button()
        Me.txtClippySearch = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.tabDD = New System.Windows.Forms.TabPage()
        Me.btnDDExit = New System.Windows.Forms.Button()
        Me.optCompanyCorr = New System.Windows.Forms.CheckBox()
        Me.txtMessageFilename = New System.Windows.Forms.TextBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnGVClear = New System.Windows.Forms.Button()
        Me.btnFilesSave = New System.Windows.Forms.Button()
        Me.dgFiles = New System.Windows.Forms.DataGridView()
        Me.OriginalFilename = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Filenames = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FileDestination = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.tabTimeGrade = New System.Windows.Forms.TabPage()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dgHistory = New System.Windows.Forms.DataGridView()
        Me.dgSummary = New System.Windows.Forms.DataGridView()
        Me.btnGradeExit = New System.Windows.Forms.Button()
        Me.btnSection = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgGrade = New System.Windows.Forms.DataGridView()
        Me.tabSS = New System.Windows.Forms.TabPage()
        Me.chkIDR = New System.Windows.Forms.CheckBox()
        Me.btnDirRefresh = New System.Windows.Forms.Button()
        Me.btnSecureSend = New System.Windows.Forms.Button()
        Me.tvwCompCorr2 = New System.Windows.Forms.TreeView()
        Me.tvwCompCorr = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence2 = New System.Windows.Forms.TreeView()
        Me.tvwDrawings2 = New System.Windows.Forms.TreeView()
        Me.tvwClientAttachments2 = New System.Windows.Forms.TreeView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboDir2 = New System.Windows.Forms.ComboBox()
        Me.tvwClientAttachments = New System.Windows.Forms.TreeView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDir = New System.Windows.Forms.ComboBox()
        Me.tvwDrawings = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence = New System.Windows.Forms.TreeView()
        Me.tabCI = New System.Windows.Forms.TabPage()
        Me.btnIssueCancel = New System.Windows.Forms.Button()
        Me.chkNoDates = New System.Windows.Forms.CheckBox()
        Me.btnExportCI = New System.Windows.Forms.Button()
        Me.btnNewIssue = New System.Windows.Forms.Button()
        Me.btnSaveCI = New System.Windows.Forms.Button()
        Me.dgContinuingIssues = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EntryDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Issue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EditDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DeletedBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DeletedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Deleted = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtCI = New System.Windows.Forms.TextBox()
        Me.tabNotes = New System.Windows.Forms.TabPage()
        Me.btnUnlockPN = New System.Windows.Forms.Button()
        Me.btnCreatePN = New System.Windows.Forms.Button()
        Me.btnSavePN = New System.Windows.Forms.Button()
        Me.txtNotes = New System.Windows.Forms.TextBox()
        Me.tabCheckList = New System.Windows.Forms.TabPage()
        Me.tvIssues = New System.Windows.Forms.TreeView()
        Me.txtIssueName = New System.Windows.Forms.TextBox()
        Me.txtIssueID = New System.Windows.Forms.TextBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.btnUpdateIssue = New System.Windows.Forms.Button()
        Me.lblItemCount = New System.Windows.Forms.Label()
        Me.btnAddIssue = New System.Windows.Forms.Button()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.ValCheckList = New System.Windows.Forms.CheckedListBox()
        Me.tabCorr = New System.Windows.Forms.TabPage()
        Me.btnPolishRpt = New System.Windows.Forms.Button()
        Me.btnRAM = New System.Windows.Forms.Button()
        Me.btnVI = New System.Windows.Forms.Button()
        Me.btnSC = New System.Windows.Forms.Button()
        Me.btnPA = New System.Windows.Forms.Button()
        Me.btnCT = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnDrawings = New System.Windows.Forms.Button()
        Me.btnPT = New System.Windows.Forms.Button()
        Me.btnSpecFrac = New System.Windows.Forms.Button()
        Me.btnUnitReview = New System.Windows.Forms.Button()
        Me.lstReturnFiles = New System.Windows.Forms.ListBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.lstVRFiles = New System.Windows.Forms.ListBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.lstVFFiles = New System.Windows.Forms.ListBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.lstVFNumbers = New System.Windows.Forms.ListBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.tabContacts = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.pnlCompany = New System.Windows.Forms.Panel()
        Me.ICEmail = New System.Windows.Forms.PictureBox()
        Me.ACEmail2 = New System.Windows.Forms.PictureBox()
        Me.CCEmail1 = New System.Windows.Forms.PictureBox()
        Me.lblInterimPhone = New System.Windows.Forms.TextBox()
        Me.lblAltPhone = New System.Windows.Forms.TextBox()
        Me.lblPhone = New System.Windows.Forms.TextBox()
        Me.lblInterimEmail = New System.Windows.Forms.TextBox()
        Me.lblAltEmail = New System.Windows.Forms.TextBox()
        Me.lblEmail = New System.Windows.Forms.TextBox()
        Me.lblInterimName = New System.Windows.Forms.TextBox()
        Me.lblAltName = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.TextBox()
        Me.btnShowIntCo = New System.Windows.Forms.Button()
        Me.btnShowAltCo = New System.Windows.Forms.Button()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.MainPanel = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PCCEmail = New System.Windows.Forms.PictureBox()
        Me.lblPCCEmail = New System.Windows.Forms.TextBox()
        Me.lblPCCName = New System.Windows.Forms.TextBox()
        Me.label = New System.Windows.Forms.Label()
        Me.pnlRefinery = New System.Windows.Forms.Panel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblPricingPhone = New System.Windows.Forms.TextBox()
        Me.PCEmail = New System.Windows.Forms.PictureBox()
        Me.lblPricingContactEmail = New System.Windows.Forms.TextBox()
        Me.lblPricingContact = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TMEmail = New System.Windows.Forms.PictureBox()
        Me.MMEmail = New System.Windows.Forms.PictureBox()
        Me.OMEmail = New System.Windows.Forms.PictureBox()
        Me.RMEmail = New System.Windows.Forms.PictureBox()
        Me.RACEmail = New System.Windows.Forms.PictureBox()
        Me.RCEmail = New System.Windows.Forms.PictureBox()
        Me.lblRefAltPhone = New System.Windows.Forms.TextBox()
        Me.lblRefCoPhone = New System.Windows.Forms.TextBox()
        Me.lblTechMgrEmail = New System.Windows.Forms.TextBox()
        Me.lblMaintMgrEmail = New System.Windows.Forms.TextBox()
        Me.lblOpsMgrEmail = New System.Windows.Forms.TextBox()
        Me.lblRefMgrEmail = New System.Windows.Forms.TextBox()
        Me.lblRefAltEmail = New System.Windows.Forms.TextBox()
        Me.lblRefCoEmail = New System.Windows.Forms.TextBox()
        Me.lblTechMgrName = New System.Windows.Forms.TextBox()
        Me.lblMaintMgrName = New System.Windows.Forms.TextBox()
        Me.lblOpsMgrName = New System.Windows.Forms.TextBox()
        Me.lblRefMgrName = New System.Windows.Forms.TextBox()
        Me.lblRefAltName = New System.Windows.Forms.TextBox()
        Me.lblRefCoName = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ConsoleTabs = New System.Windows.Forms.TabControl()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.tabQA.SuspendLayout()
        Me.tabClippy.SuspendLayout()
        CType(Me.dgClippyResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDD.SuspendLayout()
        CType(Me.dgFiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabTimeGrade.SuspendLayout()
        CType(Me.dgHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSS.SuspendLayout()
        Me.tabCI.SuspendLayout()
        CType(Me.dgContinuingIssues, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabNotes.SuspendLayout()
        Me.tabCheckList.SuspendLayout()
        Me.tabCorr.SuspendLayout()
        Me.tabContacts.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlCompany.SuspendLayout()
        CType(Me.ICEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACEmail2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CCEmail1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainPanel.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PCCEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlRefinery.SuspendLayout()
        CType(Me.PCEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TMEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MMEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OMEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RMEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RACEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RCEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConsoleTabs.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(441, 5)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(27, 23)
        Me.btnHelp.TabIndex = 32
        Me.VersionToolTip.SetToolTip(Me.btnHelp, "Documentation")
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "danger.jpg")
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.SystemColors.Control
        Me.txtVersion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersion.Location = New System.Drawing.Point(783, 702)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.ReadOnly = True
        Me.txtVersion.Size = New System.Drawing.Size(205, 15)
        Me.txtVersion.TabIndex = 39
        Me.VersionToolTip.SetToolTip(Me.txtVersion, "This is the new version")
        '
        'btnHYC
        '
        Me.btnHYC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnHYC.Enabled = False
        Me.btnHYC.Location = New System.Drawing.Point(846, 198)
        Me.btnHYC.Margin = New System.Windows.Forms.Padding(1)
        Me.btnHYC.Name = "btnHYC"
        Me.btnHYC.Size = New System.Drawing.Size(109, 30)
        Me.btnHYC.TabIndex = 104
        Me.btnHYC.Text = "HC Details"
        Me.VersionToolTip.SetToolTip(Me.btnHYC, "Hydrocracker Details")
        Me.btnHYC.UseVisualStyleBackColor = True
        '
        'btnKillProcesses
        '
        Me.btnKillProcesses.Image = CType(resources.GetObject("btnKillProcesses.Image"), System.Drawing.Image)
        Me.btnKillProcesses.Location = New System.Drawing.Point(474, 5)
        Me.btnKillProcesses.Name = "btnKillProcesses"
        Me.btnKillProcesses.Size = New System.Drawing.Size(27, 23)
        Me.btnKillProcesses.TabIndex = 42
        Me.VersionToolTip.SetToolTip(Me.btnKillProcesses, "Excel and Word Process Killer - this will kill all running processes of Excel and" & _
        " Word")
        Me.btnKillProcesses.UseVisualStyleBackColor = True
        '
        'btnSendPrelimPricing
        '
        Me.btnSendPrelimPricing.Location = New System.Drawing.Point(408, 35)
        Me.btnSendPrelimPricing.Name = "btnSendPrelimPricing"
        Me.btnSendPrelimPricing.Size = New System.Drawing.Size(84, 22)
        Me.btnSendPrelimPricing.TabIndex = 43
        Me.btnSendPrelimPricing.Text = "Prelim Pricing"
        Me.VersionToolTip.SetToolTip(Me.btnSendPrelimPricing, "Send Preliminary Pricing")
        Me.btnSendPrelimPricing.UseVisualStyleBackColor = True
        '
        'btnCreateMissingPNFiles
        '
        Me.btnCreateMissingPNFiles.Location = New System.Drawing.Point(320, 35)
        Me.btnCreateMissingPNFiles.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCreateMissingPNFiles.Name = "btnCreateMissingPNFiles"
        Me.btnCreateMissingPNFiles.Size = New System.Drawing.Size(84, 23)
        Me.btnCreateMissingPNFiles.TabIndex = 45
        Me.btnCreateMissingPNFiles.Text = "Missing PNs"
        Me.VersionToolTip.SetToolTip(Me.btnCreateMissingPNFiles, "Create all missing Presenter Note documents")
        Me.btnCreateMissingPNFiles.UseVisualStyleBackColor = True
        '
        'btnBug
        '
        Me.btnBug.Image = CType(resources.GetObject("btnBug.Image"), System.Drawing.Image)
        Me.btnBug.Location = New System.Drawing.Point(408, 5)
        Me.btnBug.Name = "btnBug"
        Me.btnBug.Size = New System.Drawing.Size(27, 23)
        Me.btnBug.TabIndex = 46
        Me.VersionToolTip.SetToolTip(Me.btnBug, "Use this to enter a bug you have found.  Please enter all steps to reproduce the " & _
        "error including Refnum chosen.")
        Me.btnBug.UseVisualStyleBackColor = True
        '
        'btnFLValidation
        '
        Me.btnFLValidation.Location = New System.Drawing.Point(408, 11)
        Me.btnFLValidation.Name = "btnFLValidation"
        Me.btnFLValidation.Size = New System.Drawing.Size(84, 23)
        Me.btnFLValidation.TabIndex = 54
        Me.btnFLValidation.Text = "FL Review"
        Me.VersionToolTip.SetToolTip(Me.btnFLValidation, "Fuel Lube Combo Review")
        Me.btnFLValidation.UseVisualStyleBackColor = True
        Me.btnFLValidation.Visible = False
        '
        'btnValFax
        '
        Me.btnValFax.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(112, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnValFax.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValFax.ForeColor = System.Drawing.Color.White
        Me.btnValFax.Location = New System.Drawing.Point(117, 12)
        Me.btnValFax.Name = "btnValFax"
        Me.btnValFax.Size = New System.Drawing.Size(84, 46)
        Me.btnValFax.TabIndex = 53
        Me.btnValFax.Text = "New VF"
        Me.VersionToolTip.SetToolTip(Me.btnValFax, "Create a new Vetted Fax")
        Me.btnValFax.UseVisualStyleBackColor = False
        '
        'btnJustLooking
        '
        Me.btnJustLooking.Location = New System.Drawing.Point(320, 11)
        Me.btnJustLooking.Name = "btnJustLooking"
        Me.btnJustLooking.Size = New System.Drawing.Size(84, 23)
        Me.btnJustLooking.TabIndex = 49
        Me.btnJustLooking.Text = "Just Looking"
        Me.VersionToolTip.SetToolTip(Me.btnJustLooking, "IDR without Calculations")
        Me.btnJustLooking.UseVisualStyleBackColor = True
        '
        'btnSS
        '
        Me.btnSS.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSS.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSS.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.btnSS.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSS.ForeColor = System.Drawing.Color.White
        Me.btnSS.Location = New System.Drawing.Point(223, 12)
        Me.btnSS.Name = "btnSS"
        Me.btnSS.Size = New System.Drawing.Size(84, 46)
        Me.btnSS.TabIndex = 51
        Me.btnSS.Text = "Secure Send"
        Me.VersionToolTip.SetToolTip(Me.btnSS, "Securely send emails to clients encrypting all attachments")
        Me.btnSS.UseVisualStyleBackColor = False
        '
        'btnValidate
        '
        Me.btnValidate.BackColor = System.Drawing.Color.Green
        Me.btnValidate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidate.ForeColor = System.Drawing.Color.White
        Me.btnValidate.Location = New System.Drawing.Point(12, 11)
        Me.btnValidate.Name = "btnValidate"
        Me.btnValidate.Size = New System.Drawing.Size(84, 47)
        Me.btnValidate.TabIndex = 50
        Me.btnValidate.Text = "IDR"
        Me.VersionToolTip.SetToolTip(Me.btnValidate, "Internal Data Review")
        Me.btnValidate.UseVisualStyleBackColor = False
        '
        'btnReturnPW
        '
        Me.btnReturnPW.Location = New System.Drawing.Point(275, 151)
        Me.btnReturnPW.Name = "btnReturnPW"
        Me.btnReturnPW.Size = New System.Drawing.Size(161, 23)
        Me.btnReturnPW.TabIndex = 81
        Me.VersionToolTip.SetToolTip(Me.btnReturnPW, "Copies Return File Password to the clipboard")
        Me.btnReturnPW.UseVisualStyleBackColor = True
        '
        'btnCompanyPW
        '
        Me.btnCompanyPW.Location = New System.Drawing.Point(79, 151)
        Me.btnCompanyPW.Name = "btnCompanyPW"
        Me.btnCompanyPW.Size = New System.Drawing.Size(138, 23)
        Me.btnCompanyPW.TabIndex = 79
        Me.VersionToolTip.SetToolTip(Me.btnCompanyPW, "Copies Company Password to Clipboard")
        Me.btnCompanyPW.UseVisualStyleBackColor = True
        '
        'btnMain
        '
        Me.btnMain.Location = New System.Drawing.Point(79, 117)
        Me.btnMain.Name = "btnMain"
        Me.btnMain.Size = New System.Drawing.Size(139, 23)
        Me.btnMain.TabIndex = 78
        Me.VersionToolTip.SetToolTip(Me.btnMain, "Consultant that has spent the most time on this refnum")
        Me.btnMain.UseVisualStyleBackColor = True
        '
        'btnBuildVRFile
        '
        Me.btnBuildVRFile.Location = New System.Drawing.Point(198, 178)
        Me.btnBuildVRFile.Name = "btnBuildVRFile"
        Me.btnBuildVRFile.Size = New System.Drawing.Size(122, 23)
        Me.btnBuildVRFile.TabIndex = 46
        Me.btnBuildVRFile.Text = "Build a VR File"
        Me.VersionToolTip.SetToolTip(Me.btnBuildVRFile, "Create a new Vetted Reply Document")
        Me.btnBuildVRFile.UseVisualStyleBackColor = True
        Me.btnBuildVRFile.Visible = False
        '
        'btnReceiptAck
        '
        Me.btnReceiptAck.Location = New System.Drawing.Point(65, 178)
        Me.btnReceiptAck.Name = "btnReceiptAck"
        Me.btnReceiptAck.Size = New System.Drawing.Size(127, 23)
        Me.btnReceiptAck.TabIndex = 8
        Me.btnReceiptAck.Text = "Receipt Acknowledged"
        Me.VersionToolTip.SetToolTip(Me.btnReceiptAck, "Creates a timestamped VA File")
        Me.btnReceiptAck.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Black
        Me.lblStatus.Location = New System.Drawing.Point(9, 702)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(47, 15)
        Me.lblStatus.TabIndex = 44
        Me.lblStatus.Text = "Ready"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.btnBug)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.lblItemsRemaining)
        Me.Panel3.Controls.Add(Me.btnKillProcesses)
        Me.Panel3.Controls.Add(Me.lblBlueFlags)
        Me.Panel3.Controls.Add(Me.lblRedFlags)
        Me.Panel3.Controls.Add(Me.btnHelp)
        Me.Panel3.Controls.Add(Me.lblLastCalcDate)
        Me.Panel3.Controls.Add(Me.lblLastUpload)
        Me.Panel3.Controls.Add(Me.lblLastFileSave)
        Me.Panel3.Controls.Add(Me.lblValidationStatus)
        Me.Panel3.Location = New System.Drawing.Point(478, 12)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(510, 110)
        Me.Panel3.TabIndex = 68
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(13, 77)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(80, 17)
        Me.Label16.TabIndex = 78
        Me.Label16.Text = "Last Calc:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 57)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 17)
        Me.Label11.TabIndex = 77
        Me.Label11.Text = "Last Upload:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(13, 37)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 17)
        Me.Label7.TabIndex = 76
        Me.Label7.Text = "File Saved:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 17)
        Me.Label3.TabIndex = 75
        Me.Label3.Text = "Review Status"
        '
        'lblItemsRemaining
        '
        Me.lblItemsRemaining.AutoSize = True
        Me.lblItemsRemaining.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemsRemaining.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblItemsRemaining.Location = New System.Drawing.Point(373, 77)
        Me.lblItemsRemaining.Name = "lblItemsRemaining"
        Me.lblItemsRemaining.Size = New System.Drawing.Size(60, 16)
        Me.lblItemsRemaining.TabIndex = 74
        Me.lblItemsRemaining.Text = "10 Items "
        '
        'lblBlueFlags
        '
        Me.lblBlueFlags.AutoSize = True
        Me.lblBlueFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlueFlags.ForeColor = System.Drawing.Color.Navy
        Me.lblBlueFlags.Location = New System.Drawing.Point(373, 57)
        Me.lblBlueFlags.Name = "lblBlueFlags"
        Me.lblBlueFlags.Size = New System.Drawing.Size(96, 16)
        Me.lblBlueFlags.TabIndex = 73
        Me.lblBlueFlags.Text = "100 Blue Flags"
        '
        'lblRedFlags
        '
        Me.lblRedFlags.AutoSize = True
        Me.lblRedFlags.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedFlags.ForeColor = System.Drawing.Color.Red
        Me.lblRedFlags.Location = New System.Drawing.Point(373, 37)
        Me.lblRedFlags.Name = "lblRedFlags"
        Me.lblRedFlags.Size = New System.Drawing.Size(88, 16)
        Me.lblRedFlags.TabIndex = 72
        Me.lblRedFlags.Text = "17 Red Flags"
        '
        'lblLastCalcDate
        '
        Me.lblLastCalcDate.AutoSize = True
        Me.lblLastCalcDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastCalcDate.Location = New System.Drawing.Point(126, 78)
        Me.lblLastCalcDate.Name = "lblLastCalcDate"
        Me.lblLastCalcDate.Size = New System.Drawing.Size(125, 16)
        Me.lblLastCalcDate.TabIndex = 71
        Me.lblLastCalcDate.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastUpload
        '
        Me.lblLastUpload.AutoSize = True
        Me.lblLastUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastUpload.Location = New System.Drawing.Point(126, 58)
        Me.lblLastUpload.Name = "lblLastUpload"
        Me.lblLastUpload.Size = New System.Drawing.Size(125, 16)
        Me.lblLastUpload.TabIndex = 70
        Me.lblLastUpload.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastFileSave
        '
        Me.lblLastFileSave.AutoSize = True
        Me.lblLastFileSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastFileSave.Location = New System.Drawing.Point(126, 38)
        Me.lblLastFileSave.Name = "lblLastFileSave"
        Me.lblLastFileSave.Size = New System.Drawing.Size(125, 16)
        Me.lblLastFileSave.TabIndex = 69
        Me.lblLastFileSave.Text = "6/8/2010 3:51:08 PM"
        '
        'lblValidationStatus
        '
        Me.lblValidationStatus.AutoSize = True
        Me.lblValidationStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidationStatus.Location = New System.Drawing.Point(126, 11)
        Me.lblValidationStatus.Name = "lblValidationStatus"
        Me.lblValidationStatus.Size = New System.Drawing.Size(71, 16)
        Me.lblValidationStatus.TabIndex = 68
        Me.lblValidationStatus.Text = "Reviewing"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label21)
        Me.Panel4.Controls.Add(Me.Label19)
        Me.Panel4.Controls.Add(Me.btnFLValidation)
        Me.Panel4.Controls.Add(Me.btnValFax)
        Me.Panel4.Controls.Add(Me.btnJustLooking)
        Me.Panel4.Controls.Add(Me.btnSS)
        Me.Panel4.Controls.Add(Me.btnValidate)
        Me.Panel4.Controls.Add(Me.btnCreateMissingPNFiles)
        Me.Panel4.Controls.Add(Me.btnSendPrelimPricing)
        Me.Panel4.Location = New System.Drawing.Point(478, 128)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(510, 77)
        Me.Panel4.TabIndex = 69
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(205, 30)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(18, 13)
        Me.Label21.TabIndex = 56
        Me.Label21.Text = "->"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(98, 29)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(18, 13)
        Me.Label19.TabIndex = 55
        Me.Label19.Text = "->"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.lblCombo)
        Me.Panel5.Controls.Add(Me.btnReturnPW)
        Me.Panel5.Controls.Add(Me.Label17)
        Me.Panel5.Controls.Add(Me.btnCompanyPW)
        Me.Panel5.Controls.Add(Me.btnMain)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Controls.Add(Me.Label14)
        Me.Panel5.Controls.Add(Me.cboCompany)
        Me.Panel5.Controls.Add(Me.lblCompany)
        Me.Panel5.Controls.Add(Me.btnFuelLube)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Controls.Add(Me.cboConsultant)
        Me.Panel5.Controls.Add(Me.lblWarning)
        Me.Panel5.Controls.Add(Me.Label60)
        Me.Panel5.Controls.Add(Me.cboRefNum)
        Me.Panel5.Controls.Add(Me.Label59)
        Me.Panel5.Controls.Add(Me.cboStudy)
        Me.Help.SetHelpKeyword(Me.Panel5, "help\hs0001.htm")
        Me.Help.SetHelpNavigator(Me.Panel5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Panel5.Location = New System.Drawing.Point(19, 12)
        Me.Panel5.Name = "Panel5"
        Me.Help.SetShowHelp(Me.Panel5, True)
        Me.Panel5.Size = New System.Drawing.Size(444, 193)
        Me.Panel5.TabIndex = 70
        '
        'lblCombo
        '
        Me.lblCombo.AutoSize = True
        Me.lblCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCombo.Location = New System.Drawing.Point(236, 63)
        Me.lblCombo.Name = "lblCombo"
        Me.lblCombo.Size = New System.Drawing.Size(25, 13)
        Me.lblCombo.TabIndex = 82
        Me.lblCombo.Text = "<->"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(220, 154)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(62, 17)
        Me.Label17.TabIndex = 80
        Me.Label17.Text = "Return "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(2, 154)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 17)
        Me.Label2.TabIndex = 77
        Me.Label2.Text = "Company"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 17)
        Me.Label1.TabIndex = 76
        Me.Label1.Text = "Main"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(2, 124)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 13)
        Me.Label14.TabIndex = 75
        '
        'cboCompany
        '
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(79, 88)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(357, 21)
        Me.cboCompany.Sorted = True
        Me.cboCompany.TabIndex = 74
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(3, 88)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(69, 17)
        Me.lblCompany.TabIndex = 73
        Me.lblCompany.Text = "Refinery"
        '
        'btnFuelLube
        '
        Me.btnFuelLube.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFuelLube.Location = New System.Drawing.Point(275, 58)
        Me.btnFuelLube.Name = "btnFuelLube"
        Me.btnFuelLube.Size = New System.Drawing.Size(161, 23)
        Me.btnFuelLube.TabIndex = 72
        Me.btnFuelLube.Text = "Fuel/Lube"
        Me.btnFuelLube.UseVisualStyleBackColor = True
        Me.btnFuelLube.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Gainsboro
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(222, 122)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 17)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = "Curr."
        '
        'cboConsultant
        '
        Me.cboConsultant.FormattingEnabled = True
        Me.cboConsultant.Location = New System.Drawing.Point(275, 121)
        Me.cboConsultant.MaxLength = 3
        Me.cboConsultant.Name = "cboConsultant"
        Me.cboConsultant.Size = New System.Drawing.Size(161, 21)
        Me.cboConsultant.TabIndex = 70
        '
        'lblWarning
        '
        Me.lblWarning.AutoSize = True
        Me.lblWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(82, 7)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(108, 13)
        Me.lblWarning.TabIndex = 69
        Me.lblWarning.Text = "Not Current Study"
        Me.lblWarning.Visible = False
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(1, 57)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(65, 17)
        Me.Label60.TabIndex = 68
        Me.Label60.Text = "RefNum"
        '
        'cboRefNum
        '
        Me.cboRefNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRefNum.FormattingEnabled = True
        Me.cboRefNum.Location = New System.Drawing.Point(80, 59)
        Me.cboRefNum.Name = "cboRefNum"
        Me.cboRefNum.Size = New System.Drawing.Size(138, 21)
        Me.cboRefNum.TabIndex = 67
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(3, 29)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(49, 17)
        Me.Label59.TabIndex = 66
        Me.Label59.Text = "Study"
        '
        'cboStudy
        '
        Me.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStudy.FormattingEnabled = True
        Me.cboStudy.Location = New System.Drawing.Point(80, 28)
        Me.cboStudy.Name = "cboStudy"
        Me.cboStudy.Size = New System.Drawing.Size(138, 21)
        Me.cboStudy.TabIndex = 64
        '
        'Help
        '
        Me.Help.HelpNamespace = "help\Console2014.chm"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(15, 19)
        Me.Label46.Name = "Label46"
        Me.Help.SetShowHelp(Me.Label46, True)
        Me.Label46.Size = New System.Drawing.Size(120, 16)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Presenter Notes"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(8, 8)
        Me.Label18.Name = "Label18"
        Me.Help.SetShowHelp(Me.Label18, True)
        Me.Label18.Size = New System.Drawing.Size(136, 17)
        Me.Label18.TabIndex = 31
        Me.Label18.Text = "Continuing Issues"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(8, 259)
        Me.Label29.Name = "Label29"
        Me.Help.SetShowHelp(Me.Label29, True)
        Me.Label29.Size = New System.Drawing.Size(51, 17)
        Me.Label29.TabIndex = 33
        Me.Label29.Text = "Issue:"
        '
        'tabQA
        '
        Me.tabQA.Controls.Add(Me.tv)
        Me.tabQA.Controls.Add(Me.chkUseRefnum)
        Me.tabQA.Controls.Add(Me.QASearch)
        Me.tabQA.Controls.Add(Me.Label22)
        Me.tabQA.Controls.Add(Me.lnkResultsPresentation)
        Me.tabQA.Controls.Add(Me.lnkPricing)
        Me.tabQA.Controls.Add(Me.lnkMisc)
        Me.tabQA.Controls.Add(Me.lnkProcessData)
        Me.tabQA.Controls.Add(Me.lnkOpex)
        Me.tabQA.Controls.Add(Me.lnkMaintenance)
        Me.tabQA.Controls.Add(Me.lnkPersonnel)
        Me.tabQA.Controls.Add(Me.lnkEnergy)
        Me.tabQA.Controls.Add(Me.lnkMaterialBalance)
        Me.tabQA.Controls.Add(Me.lnkStudyBoundary)
        Me.tabQA.Controls.Add(Me.lnkRefineryHistory)
        Me.tabQA.Location = New System.Drawing.Point(4, 22)
        Me.tabQA.Name = "tabQA"
        Me.tabQA.Padding = New System.Windows.Forms.Padding(3)
        Me.tabQA.Size = New System.Drawing.Size(972, 458)
        Me.tabQA.TabIndex = 14
        Me.tabQA.Text = "Q & A"
        Me.tabQA.UseVisualStyleBackColor = True
        '
        'tv
        '
        Me.tv.Location = New System.Drawing.Point(25, 93)
        Me.tv.Name = "tv"
        Me.tv.Scrollable = False
        Me.tv.Size = New System.Drawing.Size(914, 329)
        Me.tv.TabIndex = 86
        '
        'chkUseRefnum
        '
        Me.chkUseRefnum.AutoSize = True
        Me.chkUseRefnum.Location = New System.Drawing.Point(92, 48)
        Me.chkUseRefnum.Name = "chkUseRefnum"
        Me.chkUseRefnum.Size = New System.Drawing.Size(85, 17)
        Me.chkUseRefnum.TabIndex = 85
        Me.chkUseRefnum.Text = "Use Refnum"
        Me.chkUseRefnum.UseVisualStyleBackColor = True
        '
        'QASearch
        '
        Me.QASearch.Location = New System.Drawing.Point(92, 21)
        Me.QASearch.Name = "QASearch"
        Me.QASearch.Size = New System.Drawing.Size(225, 20)
        Me.QASearch.TabIndex = 83
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(22, 21)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(64, 17)
        Me.Label22.TabIndex = 82
        Me.Label22.Text = "Search:"
        '
        'lnkResultsPresentation
        '
        Me.lnkResultsPresentation.AutoSize = True
        Me.lnkResultsPresentation.Location = New System.Drawing.Point(780, 48)
        Me.lnkResultsPresentation.Name = "lnkResultsPresentation"
        Me.lnkResultsPresentation.Size = New System.Drawing.Size(104, 13)
        Me.lnkResultsPresentation.TabIndex = 10
        Me.lnkResultsPresentation.TabStop = True
        Me.lnkResultsPresentation.Tag = "Results Presentation"
        Me.lnkResultsPresentation.Text = "Results Presentation"
        '
        'lnkPricing
        '
        Me.lnkPricing.AutoSize = True
        Me.lnkPricing.Location = New System.Drawing.Point(690, 48)
        Me.lnkPricing.Name = "lnkPricing"
        Me.lnkPricing.Size = New System.Drawing.Size(39, 13)
        Me.lnkPricing.TabIndex = 9
        Me.lnkPricing.TabStop = True
        Me.lnkPricing.Tag = "Pricing"
        Me.lnkPricing.Text = "Pricing"
        '
        'lnkMisc
        '
        Me.lnkMisc.AutoSize = True
        Me.lnkMisc.Location = New System.Drawing.Point(569, 48)
        Me.lnkMisc.Name = "lnkMisc"
        Me.lnkMisc.Size = New System.Drawing.Size(74, 13)
        Me.lnkMisc.TabIndex = 8
        Me.lnkMisc.TabStop = True
        Me.lnkMisc.Tag = "Miscellaneous"
        Me.lnkMisc.Text = "Miscellaneous"
        '
        'lnkProcessData
        '
        Me.lnkProcessData.AutoSize = True
        Me.lnkProcessData.Location = New System.Drawing.Point(453, 48)
        Me.lnkProcessData.Name = "lnkProcessData"
        Me.lnkProcessData.Size = New System.Drawing.Size(71, 13)
        Me.lnkProcessData.TabIndex = 7
        Me.lnkProcessData.TabStop = True
        Me.lnkProcessData.Tag = "Process Data"
        Me.lnkProcessData.Text = "Process Data"
        '
        'lnkOpex
        '
        Me.lnkOpex.AutoSize = True
        Me.lnkOpex.Location = New System.Drawing.Point(341, 48)
        Me.lnkOpex.Name = "lnkOpex"
        Me.lnkOpex.Size = New System.Drawing.Size(102, 13)
        Me.lnkOpex.TabIndex = 6
        Me.lnkOpex.TabStop = True
        Me.lnkOpex.Tag = "Operating Expenses"
        Me.lnkOpex.Text = "Operating Expenses"
        '
        'lnkMaintenance
        '
        Me.lnkMaintenance.AutoSize = True
        Me.lnkMaintenance.Location = New System.Drawing.Point(870, 25)
        Me.lnkMaintenance.Name = "lnkMaintenance"
        Me.lnkMaintenance.Size = New System.Drawing.Size(69, 13)
        Me.lnkMaintenance.TabIndex = 5
        Me.lnkMaintenance.TabStop = True
        Me.lnkMaintenance.Tag = "Maintenance"
        Me.lnkMaintenance.Text = "Maintenance"
        '
        'lnkPersonnel
        '
        Me.lnkPersonnel.AutoSize = True
        Me.lnkPersonnel.Location = New System.Drawing.Point(780, 23)
        Me.lnkPersonnel.Name = "lnkPersonnel"
        Me.lnkPersonnel.Size = New System.Drawing.Size(54, 13)
        Me.lnkPersonnel.TabIndex = 4
        Me.lnkPersonnel.TabStop = True
        Me.lnkPersonnel.Tag = "Personnel"
        Me.lnkPersonnel.Text = "Personnel"
        '
        'lnkEnergy
        '
        Me.lnkEnergy.AutoSize = True
        Me.lnkEnergy.Location = New System.Drawing.Point(690, 23)
        Me.lnkEnergy.Name = "lnkEnergy"
        Me.lnkEnergy.Size = New System.Drawing.Size(40, 13)
        Me.lnkEnergy.TabIndex = 3
        Me.lnkEnergy.TabStop = True
        Me.lnkEnergy.Tag = "Energy"
        Me.lnkEnergy.Text = "Energy"
        '
        'lnkMaterialBalance
        '
        Me.lnkMaterialBalance.AutoSize = True
        Me.lnkMaterialBalance.Location = New System.Drawing.Point(569, 23)
        Me.lnkMaterialBalance.Name = "lnkMaterialBalance"
        Me.lnkMaterialBalance.Size = New System.Drawing.Size(86, 13)
        Me.lnkMaterialBalance.TabIndex = 2
        Me.lnkMaterialBalance.TabStop = True
        Me.lnkMaterialBalance.Tag = "Material Balance"
        Me.lnkMaterialBalance.Text = "Material Balance"
        '
        'lnkStudyBoundary
        '
        Me.lnkStudyBoundary.AutoSize = True
        Me.lnkStudyBoundary.Location = New System.Drawing.Point(453, 23)
        Me.lnkStudyBoundary.Name = "lnkStudyBoundary"
        Me.lnkStudyBoundary.Size = New System.Drawing.Size(82, 13)
        Me.lnkStudyBoundary.TabIndex = 1
        Me.lnkStudyBoundary.TabStop = True
        Me.lnkStudyBoundary.Tag = "Study Boundary"
        Me.lnkStudyBoundary.Text = "Study Boundary"
        '
        'lnkRefineryHistory
        '
        Me.lnkRefineryHistory.AutoSize = True
        Me.lnkRefineryHistory.Location = New System.Drawing.Point(341, 23)
        Me.lnkRefineryHistory.Name = "lnkRefineryHistory"
        Me.lnkRefineryHistory.Size = New System.Drawing.Size(81, 13)
        Me.lnkRefineryHistory.TabIndex = 0
        Me.lnkRefineryHistory.TabStop = True
        Me.lnkRefineryHistory.Tag = "Refinery History"
        Me.lnkRefineryHistory.Text = "Refinery History"
        '
        'tabClippy
        '
        Me.tabClippy.Controls.Add(Me.btnQueryQuit)
        Me.tabClippy.Controls.Add(Me.lblError)
        Me.tabClippy.Controls.Add(Me.chkSQL)
        Me.tabClippy.Controls.Add(Me.dgClippyResults)
        Me.tabClippy.Controls.Add(Me.btnClippySearch)
        Me.tabClippy.Controls.Add(Me.txtClippySearch)
        Me.tabClippy.Controls.Add(Me.Label25)
        Me.tabClippy.Location = New System.Drawing.Point(4, 22)
        Me.tabClippy.Name = "tabClippy"
        Me.tabClippy.Size = New System.Drawing.Size(972, 458)
        Me.tabClippy.TabIndex = 11
        Me.tabClippy.Text = "Query"
        Me.tabClippy.UseVisualStyleBackColor = True
        '
        'btnQueryQuit
        '
        Me.btnQueryQuit.Location = New System.Drawing.Point(870, 433)
        Me.btnQueryQuit.Name = "btnQueryQuit"
        Me.btnQueryQuit.Size = New System.Drawing.Size(69, 23)
        Me.btnQueryQuit.TabIndex = 46
        Me.btnQueryQuit.Text = "Exit"
        Me.btnQueryQuit.UseVisualStyleBackColor = True
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblError.ForeColor = System.Drawing.Color.DarkRed
        Me.lblError.Location = New System.Drawing.Point(0, 445)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 13)
        Me.lblError.TabIndex = 45
        '
        'chkSQL
        '
        Me.chkSQL.AutoSize = True
        Me.chkSQL.Checked = True
        Me.chkSQL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSQL.Location = New System.Drawing.Point(111, 133)
        Me.chkSQL.Name = "chkSQL"
        Me.chkSQL.Size = New System.Drawing.Size(47, 17)
        Me.chkSQL.TabIndex = 44
        Me.chkSQL.Text = "SQL"
        Me.chkSQL.UseVisualStyleBackColor = True
        '
        'dgClippyResults
        '
        Me.dgClippyResults.AllowUserToAddRows = False
        Me.dgClippyResults.AllowUserToDeleteRows = False
        Me.dgClippyResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgClippyResults.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgClippyResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgClippyResults.Location = New System.Drawing.Point(21, 158)
        Me.dgClippyResults.Name = "dgClippyResults"
        Me.dgClippyResults.ReadOnly = True
        Me.dgClippyResults.Size = New System.Drawing.Size(918, 274)
        Me.dgClippyResults.TabIndex = 43
        '
        'btnClippySearch
        '
        Me.btnClippySearch.Location = New System.Drawing.Point(21, 129)
        Me.btnClippySearch.Name = "btnClippySearch"
        Me.btnClippySearch.Size = New System.Drawing.Size(69, 23)
        Me.btnClippySearch.TabIndex = 42
        Me.btnClippySearch.Text = "Search"
        Me.btnClippySearch.UseVisualStyleBackColor = True
        '
        'txtClippySearch
        '
        Me.txtClippySearch.Location = New System.Drawing.Point(21, 34)
        Me.txtClippySearch.Multiline = True
        Me.txtClippySearch.Name = "txtClippySearch"
        Me.txtClippySearch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtClippySearch.Size = New System.Drawing.Size(918, 89)
        Me.txtClippySearch.TabIndex = 39
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(8, 11)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(83, 17)
        Me.Label25.TabIndex = 38
        Me.Label25.Text = "SQL Query:"
        '
        'tabDD
        '
        Me.tabDD.BackColor = System.Drawing.Color.AliceBlue
        Me.tabDD.Controls.Add(Me.btnDDExit)
        Me.tabDD.Controls.Add(Me.optCompanyCorr)
        Me.tabDD.Controls.Add(Me.txtMessageFilename)
        Me.tabDD.Controls.Add(Me.lblMessage)
        Me.tabDD.Controls.Add(Me.btnGVClear)
        Me.tabDD.Controls.Add(Me.btnFilesSave)
        Me.tabDD.Controls.Add(Me.dgFiles)
        Me.tabDD.Location = New System.Drawing.Point(4, 22)
        Me.tabDD.Name = "tabDD"
        Me.tabDD.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDD.Size = New System.Drawing.Size(972, 458)
        Me.tabDD.TabIndex = 8
        Me.tabDD.Text = "Drag and Drop"
        '
        'btnDDExit
        '
        Me.btnDDExit.Location = New System.Drawing.Point(685, 428)
        Me.btnDDExit.Name = "btnDDExit"
        Me.btnDDExit.Size = New System.Drawing.Size(80, 23)
        Me.btnDDExit.TabIndex = 51
        Me.btnDDExit.Text = "Exit"
        Me.btnDDExit.UseVisualStyleBackColor = True
        '
        'optCompanyCorr
        '
        Me.optCompanyCorr.AutoSize = True
        Me.optCompanyCorr.Location = New System.Drawing.Point(788, 32)
        Me.optCompanyCorr.Name = "optCompanyCorr"
        Me.optCompanyCorr.Size = New System.Drawing.Size(151, 17)
        Me.optCompanyCorr.TabIndex = 38
        Me.optCompanyCorr.Text = "Company Correspondence"
        Me.optCompanyCorr.UseVisualStyleBackColor = True
        '
        'txtMessageFilename
        '
        Me.txtMessageFilename.Location = New System.Drawing.Point(209, 29)
        Me.txtMessageFilename.Name = "txtMessageFilename"
        Me.txtMessageFilename.Size = New System.Drawing.Size(557, 20)
        Me.txtMessageFilename.TabIndex = 37
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(20, 29)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(183, 17)
        Me.lblMessage.TabIndex = 36
        Me.lblMessage.Text = "Filename for Email Text:"
        '
        'btnGVClear
        '
        Me.btnGVClear.Location = New System.Drawing.Point(779, 429)
        Me.btnGVClear.Name = "btnGVClear"
        Me.btnGVClear.Size = New System.Drawing.Size(75, 23)
        Me.btnGVClear.TabIndex = 2
        Me.btnGVClear.Text = "Clear"
        Me.btnGVClear.UseVisualStyleBackColor = True
        '
        'btnFilesSave
        '
        Me.btnFilesSave.Location = New System.Drawing.Point(872, 429)
        Me.btnFilesSave.Name = "btnFilesSave"
        Me.btnFilesSave.Size = New System.Drawing.Size(75, 23)
        Me.btnFilesSave.TabIndex = 1
        Me.btnFilesSave.Text = "Save"
        Me.btnFilesSave.UseVisualStyleBackColor = True
        '
        'dgFiles
        '
        Me.dgFiles.AllowUserToAddRows = False
        Me.dgFiles.AllowUserToDeleteRows = False
        Me.dgFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgFiles.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OriginalFilename, Me.Filenames, Me.FileDestination})
        Me.dgFiles.Location = New System.Drawing.Point(23, 63)
        Me.dgFiles.Name = "dgFiles"
        Me.dgFiles.Size = New System.Drawing.Size(924, 359)
        Me.dgFiles.TabIndex = 0
        '
        'OriginalFilename
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.OriginalFilename.DefaultCellStyle = DataGridViewCellStyle1
        Me.OriginalFilename.HeaderText = "Original Filename"
        Me.OriginalFilename.Name = "OriginalFilename"
        Me.OriginalFilename.ReadOnly = True
        Me.OriginalFilename.Width = 300
        '
        'Filenames
        '
        Me.Filenames.HeaderText = """Save As"" Filename"
        Me.Filenames.Name = "Filenames"
        Me.Filenames.Width = 300
        '
        'FileDestination
        '
        Me.FileDestination.DropDownWidth = 200
        Me.FileDestination.HeaderText = "Save Attachment Location"
        Me.FileDestination.Name = "FileDestination"
        Me.FileDestination.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.FileDestination.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.FileDestination.Width = 275
        '
        'tabTimeGrade
        '
        Me.tabTimeGrade.BackColor = System.Drawing.Color.AliceBlue
        Me.tabTimeGrade.Controls.Add(Me.Label15)
        Me.tabTimeGrade.Controls.Add(Me.Label10)
        Me.tabTimeGrade.Controls.Add(Me.dgHistory)
        Me.tabTimeGrade.Controls.Add(Me.dgSummary)
        Me.tabTimeGrade.Controls.Add(Me.btnGradeExit)
        Me.tabTimeGrade.Controls.Add(Me.btnSection)
        Me.tabTimeGrade.Controls.Add(Me.Label9)
        Me.tabTimeGrade.Controls.Add(Me.dgGrade)
        Me.tabTimeGrade.Location = New System.Drawing.Point(4, 22)
        Me.tabTimeGrade.Name = "tabTimeGrade"
        Me.tabTimeGrade.Padding = New System.Windows.Forms.Padding(3)
        Me.tabTimeGrade.Size = New System.Drawing.Size(972, 458)
        Me.tabTimeGrade.TabIndex = 5
        Me.tabTimeGrade.Text = "Time/Grade"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(492, 239)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(59, 17)
        Me.Label15.TabIndex = 54
        Me.Label15.Text = "History"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(20, 239)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(74, 17)
        Me.Label10.TabIndex = 53
        Me.Label10.Text = "Summary"
        '
        'dgHistory
        '
        Me.dgHistory.AllowUserToAddRows = False
        Me.dgHistory.AllowUserToDeleteRows = False
        Me.dgHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgHistory.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgHistory.Location = New System.Drawing.Point(489, 259)
        Me.dgHistory.Name = "dgHistory"
        Me.dgHistory.ReadOnly = True
        Me.dgHistory.Size = New System.Drawing.Size(460, 170)
        Me.dgHistory.TabIndex = 52
        '
        'dgSummary
        '
        Me.dgSummary.AllowUserToAddRows = False
        Me.dgSummary.AllowUserToDeleteRows = False
        Me.dgSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgSummary.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgSummary.Location = New System.Drawing.Point(23, 259)
        Me.dgSummary.Name = "dgSummary"
        Me.dgSummary.ReadOnly = True
        Me.dgSummary.Size = New System.Drawing.Size(460, 170)
        Me.dgSummary.TabIndex = 51
        '
        'btnGradeExit
        '
        Me.btnGradeExit.Location = New System.Drawing.Point(865, 431)
        Me.btnGradeExit.Name = "btnGradeExit"
        Me.btnGradeExit.Size = New System.Drawing.Size(84, 23)
        Me.btnGradeExit.TabIndex = 50
        Me.btnGradeExit.Text = "Exit"
        Me.btnGradeExit.UseVisualStyleBackColor = True
        '
        'btnSection
        '
        Me.btnSection.Location = New System.Drawing.Point(23, 6)
        Me.btnSection.Name = "btnSection"
        Me.btnSection.Size = New System.Drawing.Size(84, 23)
        Me.btnSection.TabIndex = 39
        Me.btnSection.Text = "Section"
        Me.btnSection.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(422, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 17)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Grading"
        '
        'dgGrade
        '
        Me.dgGrade.AllowUserToAddRows = False
        Me.dgGrade.AllowUserToDeleteRows = False
        Me.dgGrade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgGrade.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgGrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgGrade.Location = New System.Drawing.Point(23, 32)
        Me.dgGrade.Name = "dgGrade"
        Me.dgGrade.ReadOnly = True
        Me.dgGrade.Size = New System.Drawing.Size(926, 200)
        Me.dgGrade.TabIndex = 0
        '
        'tabSS
        '
        Me.tabSS.BackColor = System.Drawing.Color.AliceBlue
        Me.tabSS.Controls.Add(Me.chkIDR)
        Me.tabSS.Controls.Add(Me.btnDirRefresh)
        Me.tabSS.Controls.Add(Me.btnSecureSend)
        Me.tabSS.Controls.Add(Me.tvwCompCorr2)
        Me.tabSS.Controls.Add(Me.tvwCompCorr)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence2)
        Me.tabSS.Controls.Add(Me.tvwDrawings2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments2)
        Me.tabSS.Controls.Add(Me.Label13)
        Me.tabSS.Controls.Add(Me.cboDir2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments)
        Me.tabSS.Controls.Add(Me.Label12)
        Me.tabSS.Controls.Add(Me.cboDir)
        Me.tabSS.Controls.Add(Me.tvwDrawings)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence)
        Me.tabSS.Location = New System.Drawing.Point(4, 22)
        Me.tabSS.Name = "tabSS"
        Me.tabSS.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSS.Size = New System.Drawing.Size(972, 458)
        Me.tabSS.TabIndex = 7
        Me.tabSS.Text = "Secure Send"
        '
        'chkIDR
        '
        Me.chkIDR.AutoSize = True
        Me.chkIDR.Checked = True
        Me.chkIDR.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIDR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIDR.Location = New System.Drawing.Point(826, 13)
        Me.chkIDR.Name = "chkIDR"
        Me.chkIDR.Size = New System.Drawing.Size(144, 17)
        Me.chkIDR.TabIndex = 53
        Me.chkIDR.Text = "Add IDR Instructions"
        Me.chkIDR.UseVisualStyleBackColor = True
        '
        'btnDirRefresh
        '
        Me.btnDirRefresh.Location = New System.Drawing.Point(866, 428)
        Me.btnDirRefresh.Name = "btnDirRefresh"
        Me.btnDirRefresh.Size = New System.Drawing.Size(84, 23)
        Me.btnDirRefresh.TabIndex = 50
        Me.btnDirRefresh.Text = "Exit"
        Me.btnDirRefresh.UseVisualStyleBackColor = True
        '
        'btnSecureSend
        '
        Me.btnSecureSend.Location = New System.Drawing.Point(776, 428)
        Me.btnSecureSend.Name = "btnSecureSend"
        Me.btnSecureSend.Size = New System.Drawing.Size(84, 23)
        Me.btnSecureSend.TabIndex = 49
        Me.btnSecureSend.Text = "Continue"
        Me.btnSecureSend.UseVisualStyleBackColor = True
        '
        'tvwCompCorr2
        '
        Me.tvwCompCorr2.AllowDrop = True
        Me.tvwCompCorr2.CheckBoxes = True
        Me.tvwCompCorr2.ImageIndex = 0
        Me.tvwCompCorr2.ImageList = Me.ImageList1
        Me.tvwCompCorr2.Location = New System.Drawing.Point(489, 35)
        Me.tvwCompCorr2.Name = "tvwCompCorr2"
        Me.tvwCompCorr2.SelectedImageIndex = 0
        Me.tvwCompCorr2.Size = New System.Drawing.Size(461, 386)
        Me.tvwCompCorr2.TabIndex = 48
        Me.tvwCompCorr2.Visible = False
        '
        'tvwCompCorr
        '
        Me.tvwCompCorr.AllowDrop = True
        Me.tvwCompCorr.CheckBoxes = True
        Me.tvwCompCorr.ImageIndex = 0
        Me.tvwCompCorr.ImageList = Me.ImageList1
        Me.tvwCompCorr.Location = New System.Drawing.Point(17, 35)
        Me.tvwCompCorr.Name = "tvwCompCorr"
        Me.tvwCompCorr.SelectedImageIndex = 0
        Me.tvwCompCorr.Size = New System.Drawing.Size(466, 386)
        Me.tvwCompCorr.TabIndex = 47
        Me.tvwCompCorr.Visible = False
        '
        'tvwCorrespondence2
        '
        Me.tvwCorrespondence2.AllowDrop = True
        Me.tvwCorrespondence2.CheckBoxes = True
        Me.tvwCorrespondence2.ImageIndex = 0
        Me.tvwCorrespondence2.ImageList = Me.ImageList1
        Me.tvwCorrespondence2.Location = New System.Drawing.Point(489, 36)
        Me.tvwCorrespondence2.Name = "tvwCorrespondence2"
        Me.tvwCorrespondence2.SelectedImageIndex = 0
        Me.tvwCorrespondence2.Size = New System.Drawing.Size(461, 385)
        Me.tvwCorrespondence2.TabIndex = 46
        '
        'tvwDrawings2
        '
        Me.tvwDrawings2.AllowDrop = True
        Me.tvwDrawings2.CheckBoxes = True
        Me.tvwDrawings2.ImageIndex = 0
        Me.tvwDrawings2.ImageList = Me.ImageList1
        Me.tvwDrawings2.Location = New System.Drawing.Point(489, 35)
        Me.tvwDrawings2.Name = "tvwDrawings2"
        Me.tvwDrawings2.SelectedImageIndex = 0
        Me.tvwDrawings2.Size = New System.Drawing.Size(461, 386)
        Me.tvwDrawings2.TabIndex = 45
        Me.tvwDrawings2.Visible = False
        '
        'tvwClientAttachments2
        '
        Me.tvwClientAttachments2.AllowDrop = True
        Me.tvwClientAttachments2.CheckBoxes = True
        Me.tvwClientAttachments2.ImageIndex = 0
        Me.tvwClientAttachments2.ImageList = Me.ImageList1
        Me.tvwClientAttachments2.Location = New System.Drawing.Point(489, 35)
        Me.tvwClientAttachments2.Name = "tvwClientAttachments2"
        Me.tvwClientAttachments2.SelectedImageIndex = 0
        Me.tvwClientAttachments2.Size = New System.Drawing.Size(461, 386)
        Me.tvwClientAttachments2.TabIndex = 43
        Me.tvwClientAttachments2.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(471, 10)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(79, 17)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Directory:"
        '
        'cboDir2
        '
        Me.cboDir2.FormattingEnabled = True
        Me.cboDir2.Location = New System.Drawing.Point(557, 8)
        Me.cboDir2.Name = "cboDir2"
        Me.cboDir2.Size = New System.Drawing.Size(164, 21)
        Me.cboDir2.TabIndex = 41
        '
        'tvwClientAttachments
        '
        Me.tvwClientAttachments.AllowDrop = True
        Me.tvwClientAttachments.CheckBoxes = True
        Me.tvwClientAttachments.ImageIndex = 0
        Me.tvwClientAttachments.ImageList = Me.ImageList1
        Me.tvwClientAttachments.Location = New System.Drawing.Point(17, 35)
        Me.tvwClientAttachments.Name = "tvwClientAttachments"
        Me.tvwClientAttachments.SelectedImageIndex = 0
        Me.tvwClientAttachments.Size = New System.Drawing.Size(466, 385)
        Me.tvwClientAttachments.TabIndex = 40
        Me.tvwClientAttachments.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(7, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(79, 17)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "Directory:"
        '
        'cboDir
        '
        Me.cboDir.FormattingEnabled = True
        Me.cboDir.Location = New System.Drawing.Point(89, 7)
        Me.cboDir.Name = "cboDir"
        Me.cboDir.Size = New System.Drawing.Size(164, 21)
        Me.cboDir.TabIndex = 38
        '
        'tvwDrawings
        '
        Me.tvwDrawings.AllowDrop = True
        Me.tvwDrawings.CheckBoxes = True
        Me.tvwDrawings.ImageIndex = 0
        Me.tvwDrawings.ImageList = Me.ImageList1
        Me.tvwDrawings.Location = New System.Drawing.Point(17, 34)
        Me.tvwDrawings.Name = "tvwDrawings"
        Me.tvwDrawings.SelectedImageIndex = 0
        Me.tvwDrawings.Size = New System.Drawing.Size(466, 386)
        Me.tvwDrawings.TabIndex = 36
        Me.tvwDrawings.Visible = False
        '
        'tvwCorrespondence
        '
        Me.tvwCorrespondence.AllowDrop = True
        Me.tvwCorrespondence.CheckBoxes = True
        Me.tvwCorrespondence.ImageIndex = 0
        Me.tvwCorrespondence.ImageList = Me.ImageList1
        Me.tvwCorrespondence.Location = New System.Drawing.Point(17, 35)
        Me.tvwCorrespondence.Name = "tvwCorrespondence"
        Me.tvwCorrespondence.SelectedImageIndex = 0
        Me.tvwCorrespondence.Size = New System.Drawing.Size(466, 385)
        Me.tvwCorrespondence.TabIndex = 0
        '
        'tabCI
        '
        Me.tabCI.Controls.Add(Me.btnIssueCancel)
        Me.tabCI.Controls.Add(Me.Label18)
        Me.tabCI.Controls.Add(Me.chkNoDates)
        Me.tabCI.Controls.Add(Me.btnExportCI)
        Me.tabCI.Controls.Add(Me.btnNewIssue)
        Me.tabCI.Controls.Add(Me.btnSaveCI)
        Me.tabCI.Controls.Add(Me.dgContinuingIssues)
        Me.tabCI.Controls.Add(Me.txtCI)
        Me.tabCI.Controls.Add(Me.Label29)
        Me.tabCI.Location = New System.Drawing.Point(4, 22)
        Me.tabCI.Name = "tabCI"
        Me.tabCI.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCI.Size = New System.Drawing.Size(972, 458)
        Me.tabCI.TabIndex = 13
        Me.tabCI.Text = "Continuing Issues"
        Me.tabCI.UseVisualStyleBackColor = True
        '
        'btnIssueCancel
        '
        Me.btnIssueCancel.Location = New System.Drawing.Point(667, 423)
        Me.btnIssueCancel.Margin = New System.Windows.Forms.Padding(1)
        Me.btnIssueCancel.Name = "btnIssueCancel"
        Me.btnIssueCancel.Size = New System.Drawing.Size(99, 28)
        Me.btnIssueCancel.TabIndex = 32
        Me.btnIssueCancel.Text = "Cancel"
        Me.btnIssueCancel.UseVisualStyleBackColor = True
        '
        'chkNoDates
        '
        Me.chkNoDates.AutoSize = True
        Me.chkNoDates.Location = New System.Drawing.Point(119, 430)
        Me.chkNoDates.Name = "chkNoDates"
        Me.chkNoDates.Size = New System.Drawing.Size(71, 17)
        Me.chkNoDates.TabIndex = 30
        Me.chkNoDates.Text = "No Dates"
        Me.chkNoDates.UseVisualStyleBackColor = True
        '
        'btnExportCI
        '
        Me.btnExportCI.Enabled = False
        Me.btnExportCI.Location = New System.Drawing.Point(10, 423)
        Me.btnExportCI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnExportCI.Name = "btnExportCI"
        Me.btnExportCI.Size = New System.Drawing.Size(90, 28)
        Me.btnExportCI.TabIndex = 29
        Me.btnExportCI.Text = "Export to Text"
        Me.btnExportCI.UseVisualStyleBackColor = True
        '
        'btnNewIssue
        '
        Me.btnNewIssue.Location = New System.Drawing.Point(771, 423)
        Me.btnNewIssue.Margin = New System.Windows.Forms.Padding(1)
        Me.btnNewIssue.Name = "btnNewIssue"
        Me.btnNewIssue.Size = New System.Drawing.Size(95, 28)
        Me.btnNewIssue.TabIndex = 28
        Me.btnNewIssue.Text = "New Issue"
        Me.btnNewIssue.UseVisualStyleBackColor = True
        '
        'btnSaveCI
        '
        Me.btnSaveCI.Enabled = False
        Me.btnSaveCI.Location = New System.Drawing.Point(872, 423)
        Me.btnSaveCI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSaveCI.Name = "btnSaveCI"
        Me.btnSaveCI.Size = New System.Drawing.Size(87, 28)
        Me.btnSaveCI.TabIndex = 14
        Me.btnSaveCI.Text = "Save Issue"
        Me.btnSaveCI.UseVisualStyleBackColor = True
        '
        'dgContinuingIssues
        '
        Me.dgContinuingIssues.AllowUserToAddRows = False
        Me.dgContinuingIssues.AllowUserToDeleteRows = False
        Me.dgContinuingIssues.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgContinuingIssues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgContinuingIssues.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.EntryDate, Me.Issue, Me.EditDate, Me.DeletedBy, Me.DeletedDate, Me.Deleted})
        Me.dgContinuingIssues.Location = New System.Drawing.Point(9, 32)
        Me.dgContinuingIssues.Name = "dgContinuingIssues"
        Me.dgContinuingIssues.ReadOnly = True
        Me.dgContinuingIssues.RowHeadersVisible = False
        Me.dgContinuingIssues.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgContinuingIssues.Size = New System.Drawing.Size(950, 384)
        Me.dgContinuingIssues.TabIndex = 0
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'EntryDate
        '
        Me.EntryDate.HeaderText = "Entry Date"
        Me.EntryDate.Name = "EntryDate"
        Me.EntryDate.ReadOnly = True
        Me.EntryDate.Width = 150
        '
        'Issue
        '
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Issue.DefaultCellStyle = DataGridViewCellStyle2
        Me.Issue.HeaderText = "Issues"
        Me.Issue.Name = "Issue"
        Me.Issue.ReadOnly = True
        Me.Issue.Width = 700
        '
        'EditDate
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.EditDate.DefaultCellStyle = DataGridViewCellStyle3
        Me.EditDate.HeaderText = "Consultant"
        Me.EditDate.Name = "EditDate"
        Me.EditDate.ReadOnly = True
        '
        'DeletedBy
        '
        Me.DeletedBy.HeaderText = "DeletedBy"
        Me.DeletedBy.Name = "DeletedBy"
        Me.DeletedBy.ReadOnly = True
        Me.DeletedBy.Visible = False
        '
        'DeletedDate
        '
        Me.DeletedDate.HeaderText = "DeletedDate"
        Me.DeletedDate.Name = "DeletedDate"
        Me.DeletedDate.ReadOnly = True
        Me.DeletedDate.Visible = False
        '
        'Deleted
        '
        Me.Deleted.HeaderText = "Deleted"
        Me.Deleted.Name = "Deleted"
        Me.Deleted.ReadOnly = True
        Me.Deleted.Visible = False
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(11, 279)
        Me.txtCI.Multiline = True
        Me.txtCI.Name = "txtCI"
        Me.txtCI.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCI.Size = New System.Drawing.Size(948, 137)
        Me.txtCI.TabIndex = 24
        '
        'tabNotes
        '
        Me.tabNotes.BackColor = System.Drawing.Color.AliceBlue
        Me.tabNotes.Controls.Add(Me.btnUnlockPN)
        Me.tabNotes.Controls.Add(Me.btnCreatePN)
        Me.tabNotes.Controls.Add(Me.btnSavePN)
        Me.tabNotes.Controls.Add(Me.txtNotes)
        Me.tabNotes.Controls.Add(Me.Label46)
        Me.tabNotes.Location = New System.Drawing.Point(4, 22)
        Me.tabNotes.Name = "tabNotes"
        Me.tabNotes.Padding = New System.Windows.Forms.Padding(3)
        Me.tabNotes.Size = New System.Drawing.Size(972, 458)
        Me.tabNotes.TabIndex = 2
        Me.tabNotes.Text = "Notes"
        '
        'btnUnlockPN
        '
        Me.btnUnlockPN.Location = New System.Drawing.Point(858, 16)
        Me.btnUnlockPN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnlockPN.Name = "btnUnlockPN"
        Me.btnUnlockPN.Size = New System.Drawing.Size(89, 28)
        Me.btnUnlockPN.TabIndex = 34
        Me.btnUnlockPN.Text = "Unlock PN File"
        Me.btnUnlockPN.UseVisualStyleBackColor = True
        Me.btnUnlockPN.Visible = False
        '
        'btnCreatePN
        '
        Me.btnCreatePN.Location = New System.Drawing.Point(760, 16)
        Me.btnCreatePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCreatePN.Name = "btnCreatePN"
        Me.btnCreatePN.Size = New System.Drawing.Size(89, 28)
        Me.btnCreatePN.TabIndex = 33
        Me.btnCreatePN.Text = "Create PN File"
        Me.btnCreatePN.UseVisualStyleBackColor = True
        '
        'btnSavePN
        '
        Me.btnSavePN.Location = New System.Drawing.Point(875, 419)
        Me.btnSavePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSavePN.Name = "btnSavePN"
        Me.btnSavePN.Size = New System.Drawing.Size(74, 28)
        Me.btnSavePN.TabIndex = 13
        Me.btnSavePN.Text = "Save"
        Me.btnSavePN.UseVisualStyleBackColor = True
        '
        'txtNotes
        '
        Me.txtNotes.Location = New System.Drawing.Point(15, 51)
        Me.txtNotes.Multiline = True
        Me.txtNotes.Name = "txtNotes"
        Me.txtNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNotes.Size = New System.Drawing.Size(932, 355)
        Me.txtNotes.TabIndex = 12
        '
        'tabCheckList
        '
        Me.tabCheckList.BackColor = System.Drawing.Color.AliceBlue
        Me.tabCheckList.Controls.Add(Me.tvIssues)
        Me.tabCheckList.Controls.Add(Me.txtIssueName)
        Me.tabCheckList.Controls.Add(Me.txtIssueID)
        Me.tabCheckList.Controls.Add(Me.txtDescription)
        Me.tabCheckList.Controls.Add(Me.btnUpdateIssue)
        Me.tabCheckList.Controls.Add(Me.lblItemCount)
        Me.tabCheckList.Controls.Add(Me.btnAddIssue)
        Me.tabCheckList.Controls.Add(Me.Label49)
        Me.tabCheckList.Controls.Add(Me.Label48)
        Me.tabCheckList.Controls.Add(Me.Label47)
        Me.tabCheckList.Controls.Add(Me.ValCheckList)
        Me.tabCheckList.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabCheckList.Location = New System.Drawing.Point(4, 22)
        Me.tabCheckList.Name = "tabCheckList"
        Me.tabCheckList.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCheckList.Size = New System.Drawing.Size(972, 458)
        Me.tabCheckList.TabIndex = 3
        Me.tabCheckList.Text = "Checklist"
        '
        'tvIssues
        '
        Me.tvIssues.CheckBoxes = True
        Me.tvIssues.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvIssues.Location = New System.Drawing.Point(3, 38)
        Me.tvIssues.Name = "tvIssues"
        Me.tvIssues.ShowLines = False
        Me.tvIssues.ShowPlusMinus = False
        Me.tvIssues.ShowRootLines = False
        Me.tvIssues.Size = New System.Drawing.Size(399, 410)
        Me.tvIssues.TabIndex = 33
        '
        'txtIssueName
        '
        Me.txtIssueName.Location = New System.Drawing.Point(634, 54)
        Me.txtIssueName.Name = "txtIssueName"
        Me.txtIssueName.Size = New System.Drawing.Size(321, 22)
        Me.txtIssueName.TabIndex = 28
        '
        'txtIssueID
        '
        Me.txtIssueID.Location = New System.Drawing.Point(437, 54)
        Me.txtIssueID.Name = "txtIssueID"
        Me.txtIssueID.Size = New System.Drawing.Size(191, 22)
        Me.txtIssueID.TabIndex = 27
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(433, 104)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(522, 344)
        Me.txtDescription.TabIndex = 31
        '
        'btnUpdateIssue
        '
        Me.btnUpdateIssue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateIssue.Location = New System.Drawing.Point(571, 8)
        Me.btnUpdateIssue.Name = "btnUpdateIssue"
        Me.btnUpdateIssue.Size = New System.Drawing.Size(90, 23)
        Me.btnUpdateIssue.TabIndex = 26
        Me.btnUpdateIssue.Text = "Update"
        Me.btnUpdateIssue.UseVisualStyleBackColor = True
        '
        'lblItemCount
        '
        Me.lblItemCount.AutoSize = True
        Me.lblItemCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemCount.Location = New System.Drawing.Point(6, 18)
        Me.lblItemCount.Name = "lblItemCount"
        Me.lblItemCount.Size = New System.Drawing.Size(92, 17)
        Me.lblItemCount.TabIndex = 26
        Me.lblItemCount.Text = "Issue Name"
        '
        'btnAddIssue
        '
        Me.btnAddIssue.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddIssue.Location = New System.Drawing.Point(439, 8)
        Me.btnAddIssue.Name = "btnAddIssue"
        Me.btnAddIssue.Size = New System.Drawing.Size(118, 23)
        Me.btnAddIssue.TabIndex = 25
        Me.btnAddIssue.Text = "Add New Issue"
        Me.btnAddIssue.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(631, 36)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(90, 16)
        Me.Label49.TabIndex = 16
        Me.Label49.Text = "Issue Name"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(434, 38)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(64, 16)
        Me.Label48.TabIndex = 15
        Me.Label48.Text = "Issue ID"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(434, 82)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(87, 16)
        Me.Label47.TabIndex = 14
        Me.Label47.Text = "Description"
        '
        'ValCheckList
        '
        Me.ValCheckList.FormattingEnabled = True
        Me.ValCheckList.HorizontalScrollbar = True
        Me.ValCheckList.Location = New System.Drawing.Point(3, 54)
        Me.ValCheckList.Name = "ValCheckList"
        Me.ValCheckList.Size = New System.Drawing.Size(399, 378)
        Me.ValCheckList.TabIndex = 0
        '
        'tabCorr
        '
        Me.tabCorr.BackColor = System.Drawing.Color.AliceBlue
        Me.tabCorr.Controls.Add(Me.btnPolishRpt)
        Me.tabCorr.Controls.Add(Me.btnRAM)
        Me.tabCorr.Controls.Add(Me.btnVI)
        Me.tabCorr.Controls.Add(Me.btnSC)
        Me.tabCorr.Controls.Add(Me.btnPA)
        Me.tabCorr.Controls.Add(Me.btnCT)
        Me.tabCorr.Controls.Add(Me.btnRefresh)
        Me.tabCorr.Controls.Add(Me.btnDrawings)
        Me.tabCorr.Controls.Add(Me.btnHYC)
        Me.tabCorr.Controls.Add(Me.btnPT)
        Me.tabCorr.Controls.Add(Me.btnSpecFrac)
        Me.tabCorr.Controls.Add(Me.btnUnitReview)
        Me.tabCorr.Controls.Add(Me.btnBuildVRFile)
        Me.tabCorr.Controls.Add(Me.btnReceiptAck)
        Me.tabCorr.Controls.Add(Me.lstReturnFiles)
        Me.tabCorr.Controls.Add(Me.Label58)
        Me.tabCorr.Controls.Add(Me.lstVRFiles)
        Me.tabCorr.Controls.Add(Me.Label57)
        Me.tabCorr.Controls.Add(Me.lstVFFiles)
        Me.tabCorr.Controls.Add(Me.Label56)
        Me.tabCorr.Controls.Add(Me.lstVFNumbers)
        Me.tabCorr.Controls.Add(Me.Label55)
        Me.tabCorr.Location = New System.Drawing.Point(4, 22)
        Me.tabCorr.Name = "tabCorr"
        Me.tabCorr.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCorr.Size = New System.Drawing.Size(972, 458)
        Me.tabCorr.TabIndex = 4
        Me.tabCorr.Text = "Correspondence"
        '
        'btnPolishRpt
        '
        Me.btnPolishRpt.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPolishRpt.Location = New System.Drawing.Point(846, 262)
        Me.btnPolishRpt.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPolishRpt.Name = "btnPolishRpt"
        Me.btnPolishRpt.Size = New System.Drawing.Size(109, 30)
        Me.btnPolishRpt.TabIndex = 106
        Me.btnPolishRpt.Text = "Polish Report"
        Me.btnPolishRpt.UseVisualStyleBackColor = True
        '
        'btnRAM
        '
        Me.btnRAM.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnRAM.Enabled = False
        Me.btnRAM.Location = New System.Drawing.Point(846, 360)
        Me.btnRAM.Margin = New System.Windows.Forms.Padding(1)
        Me.btnRAM.Name = "btnRAM"
        Me.btnRAM.Size = New System.Drawing.Size(109, 30)
        Me.btnRAM.TabIndex = 100
        Me.btnRAM.Text = "RAM Tables (RAM)"
        Me.btnRAM.UseVisualStyleBackColor = True
        '
        'btnVI
        '
        Me.btnVI.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnVI.Enabled = False
        Me.btnVI.Location = New System.Drawing.Point(846, 392)
        Me.btnVI.Margin = New System.Windows.Forms.Padding(1)
        Me.btnVI.Name = "btnVI"
        Me.btnVI.Size = New System.Drawing.Size(109, 30)
        Me.btnVI.TabIndex = 101
        Me.btnVI.Text = "Vetted Input (VI)"
        Me.btnVI.UseVisualStyleBackColor = True
        '
        'btnSC
        '
        Me.btnSC.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSC.Enabled = False
        Me.btnSC.Location = New System.Drawing.Point(846, 70)
        Me.btnSC.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSC.Name = "btnSC"
        Me.btnSC.Size = New System.Drawing.Size(109, 30)
        Me.btnSC.TabIndex = 98
        Me.btnSC.Text = "Sum Calc (SC)"
        Me.btnSC.UseVisualStyleBackColor = True
        '
        'btnPA
        '
        Me.btnPA.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnPA.Enabled = False
        Me.btnPA.Location = New System.Drawing.Point(846, 328)
        Me.btnPA.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPA.Name = "btnPA"
        Me.btnPA.Size = New System.Drawing.Size(109, 30)
        Me.btnPA.TabIndex = 99
        Me.btnPA.Text = "Gap File (PA)"
        Me.btnPA.UseVisualStyleBackColor = True
        '
        'btnCT
        '
        Me.btnCT.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCT.Enabled = False
        Me.btnCT.Location = New System.Drawing.Point(846, 296)
        Me.btnCT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCT.Name = "btnCT"
        Me.btnCT.Size = New System.Drawing.Size(109, 30)
        Me.btnCT.TabIndex = 97
        Me.btnCT.Text = "Client Table (CT)"
        Me.btnCT.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(846, 24)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(109, 30)
        Me.btnRefresh.TabIndex = 95
        Me.btnRefresh.Text = "Open Explorer"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnDrawings
        '
        Me.btnDrawings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnDrawings.Enabled = False
        Me.btnDrawings.Location = New System.Drawing.Point(846, 230)
        Me.btnDrawings.Margin = New System.Windows.Forms.Padding(1)
        Me.btnDrawings.Name = "btnDrawings"
        Me.btnDrawings.Size = New System.Drawing.Size(109, 30)
        Me.btnDrawings.TabIndex = 105
        Me.btnDrawings.Text = "Drawings"
        Me.btnDrawings.UseVisualStyleBackColor = True
        '
        'btnPT
        '
        Me.btnPT.Enabled = False
        Me.btnPT.Location = New System.Drawing.Point(846, 102)
        Me.btnPT.Margin = New System.Windows.Forms.Padding(1)
        Me.btnPT.Name = "btnPT"
        Me.btnPT.Size = New System.Drawing.Size(109, 30)
        Me.btnPT.TabIndex = 96
        Me.btnPT.Text = "Prelim Client (PT)"
        Me.btnPT.UseVisualStyleBackColor = True
        '
        'btnSpecFrac
        '
        Me.btnSpecFrac.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnSpecFrac.Enabled = False
        Me.btnSpecFrac.Location = New System.Drawing.Point(846, 166)
        Me.btnSpecFrac.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSpecFrac.Name = "btnSpecFrac"
        Me.btnSpecFrac.Size = New System.Drawing.Size(109, 30)
        Me.btnSpecFrac.TabIndex = 103
        Me.btnSpecFrac.Text = "Spec Frac"
        Me.btnSpecFrac.UseVisualStyleBackColor = True
        '
        'btnUnitReview
        '
        Me.btnUnitReview.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnUnitReview.Enabled = False
        Me.btnUnitReview.Location = New System.Drawing.Point(846, 134)
        Me.btnUnitReview.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnitReview.Name = "btnUnitReview"
        Me.btnUnitReview.Size = New System.Drawing.Size(109, 30)
        Me.btnUnitReview.TabIndex = 102
        Me.btnUnitReview.Text = "Unit Review"
        Me.btnUnitReview.UseVisualStyleBackColor = True
        '
        'lstReturnFiles
        '
        Me.lstReturnFiles.ColumnWidth = 300
        Me.lstReturnFiles.FormattingEnabled = True
        Me.lstReturnFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstReturnFiles.Location = New System.Drawing.Point(64, 375)
        Me.lstReturnFiles.Name = "lstReturnFiles"
        Me.lstReturnFiles.Size = New System.Drawing.Size(758, 82)
        Me.lstReturnFiles.TabIndex = 7
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.Location = New System.Drawing.Point(62, 356)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(75, 13)
        Me.Label58.TabIndex = 6
        Me.Label58.Text = "Return Files"
        '
        'lstVRFiles
        '
        Me.lstVRFiles.ColumnWidth = 300
        Me.lstVRFiles.FormattingEnabled = True
        Me.lstVRFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVRFiles.Location = New System.Drawing.Point(64, 236)
        Me.lstVRFiles.Name = "lstVRFiles"
        Me.lstVRFiles.Size = New System.Drawing.Size(758, 108)
        Me.lstVRFiles.TabIndex = 5
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.Location = New System.Drawing.Point(62, 216)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(153, 13)
        Me.Label57.TabIndex = 4
        Me.Label57.Text = "Vetted Reply Files (VR*.*)"
        '
        'lstVFFiles
        '
        Me.lstVFFiles.ColumnWidth = 300
        Me.lstVFFiles.FormattingEnabled = True
        Me.lstVFFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVFFiles.Location = New System.Drawing.Point(62, 25)
        Me.lstVFFiles.Name = "lstVFFiles"
        Me.lstVFFiles.Size = New System.Drawing.Size(758, 147)
        Me.lstVFFiles.TabIndex = 3
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(59, 7)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(139, 13)
        Me.Label56.TabIndex = 2
        Me.Label56.Text = "Vetted Fax Files (VF*.*)"
        '
        'lstVFNumbers
        '
        Me.lstVFNumbers.FormattingEnabled = True
        Me.lstVFNumbers.Location = New System.Drawing.Point(12, 24)
        Me.lstVFNumbers.Name = "lstVFNumbers"
        Me.lstVFNumbers.Size = New System.Drawing.Size(41, 433)
        Me.lstVFNumbers.Sorted = True
        Me.lstVFNumbers.TabIndex = 1
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(9, 7)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(30, 13)
        Me.Label55.TabIndex = 0
        Me.Label55.Text = "VF#"
        '
        'tabContacts
        '
        Me.tabContacts.Controls.Add(Me.Panel2)
        Me.tabContacts.Controls.Add(Me.MainPanel)
        Me.tabContacts.Location = New System.Drawing.Point(4, 22)
        Me.tabContacts.Name = "tabContacts"
        Me.tabContacts.Padding = New System.Windows.Forms.Padding(3)
        Me.tabContacts.Size = New System.Drawing.Size(972, 458)
        Me.tabContacts.TabIndex = 0
        Me.tabContacts.Text = "Contacts"
        Me.tabContacts.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.pnlCompany)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(966, 129)
        Me.Panel2.TabIndex = 21
        '
        'pnlCompany
        '
        Me.pnlCompany.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.pnlCompany.AllowDrop = True
        Me.pnlCompany.BackColor = System.Drawing.Color.LightGreen
        Me.pnlCompany.Controls.Add(Me.ICEmail)
        Me.pnlCompany.Controls.Add(Me.ACEmail2)
        Me.pnlCompany.Controls.Add(Me.CCEmail1)
        Me.pnlCompany.Controls.Add(Me.lblInterimPhone)
        Me.pnlCompany.Controls.Add(Me.lblAltPhone)
        Me.pnlCompany.Controls.Add(Me.lblPhone)
        Me.pnlCompany.Controls.Add(Me.lblInterimEmail)
        Me.pnlCompany.Controls.Add(Me.lblAltEmail)
        Me.pnlCompany.Controls.Add(Me.lblEmail)
        Me.pnlCompany.Controls.Add(Me.lblInterimName)
        Me.pnlCompany.Controls.Add(Me.lblAltName)
        Me.pnlCompany.Controls.Add(Me.lblName)
        Me.pnlCompany.Controls.Add(Me.btnShowIntCo)
        Me.pnlCompany.Controls.Add(Me.btnShowAltCo)
        Me.pnlCompany.Controls.Add(Me.btnShow)
        Me.pnlCompany.Controls.Add(Me.Label6)
        Me.pnlCompany.Controls.Add(Me.Label5)
        Me.pnlCompany.Controls.Add(Me.Label4)
        Me.pnlCompany.Location = New System.Drawing.Point(4, 3)
        Me.pnlCompany.Name = "pnlCompany"
        Me.pnlCompany.Size = New System.Drawing.Size(945, 121)
        Me.pnlCompany.TabIndex = 0
        '
        'ICEmail
        '
        Me.ICEmail.Image = CType(resources.GetObject("ICEmail.Image"), System.Drawing.Image)
        Me.ICEmail.Location = New System.Drawing.Point(739, 91)
        Me.ICEmail.Name = "ICEmail"
        Me.ICEmail.Size = New System.Drawing.Size(23, 21)
        Me.ICEmail.TabIndex = 64
        Me.ICEmail.TabStop = False
        '
        'ACEmail2
        '
        Me.ACEmail2.Image = CType(resources.GetObject("ACEmail2.Image"), System.Drawing.Image)
        Me.ACEmail2.Location = New System.Drawing.Point(740, 63)
        Me.ACEmail2.Name = "ACEmail2"
        Me.ACEmail2.Size = New System.Drawing.Size(23, 21)
        Me.ACEmail2.TabIndex = 63
        Me.ACEmail2.TabStop = False
        '
        'CCEmail1
        '
        Me.CCEmail1.Image = CType(resources.GetObject("CCEmail1.Image"), System.Drawing.Image)
        Me.CCEmail1.Location = New System.Drawing.Point(740, 33)
        Me.CCEmail1.Name = "CCEmail1"
        Me.CCEmail1.Size = New System.Drawing.Size(23, 21)
        Me.CCEmail1.TabIndex = 62
        Me.CCEmail1.TabStop = False
        '
        'lblInterimPhone
        '
        Me.lblInterimPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblInterimPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterimPhone.Location = New System.Drawing.Point(769, 93)
        Me.lblInterimPhone.Name = "lblInterimPhone"
        Me.lblInterimPhone.ReadOnly = True
        Me.lblInterimPhone.Size = New System.Drawing.Size(139, 16)
        Me.lblInterimPhone.TabIndex = 61
        '
        'lblAltPhone
        '
        Me.lblAltPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblAltPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltPhone.Location = New System.Drawing.Point(769, 64)
        Me.lblAltPhone.Name = "lblAltPhone"
        Me.lblAltPhone.ReadOnly = True
        Me.lblAltPhone.Size = New System.Drawing.Size(139, 16)
        Me.lblAltPhone.TabIndex = 60
        '
        'lblPhone
        '
        Me.lblPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(769, 35)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.ReadOnly = True
        Me.lblPhone.Size = New System.Drawing.Size(139, 16)
        Me.lblPhone.TabIndex = 59
        '
        'lblInterimEmail
        '
        Me.lblInterimEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblInterimEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterimEmail.Location = New System.Drawing.Point(335, 91)
        Me.lblInterimEmail.Name = "lblInterimEmail"
        Me.lblInterimEmail.ReadOnly = True
        Me.lblInterimEmail.Size = New System.Drawing.Size(391, 16)
        Me.lblInterimEmail.TabIndex = 58
        '
        'lblAltEmail
        '
        Me.lblAltEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblAltEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltEmail.Location = New System.Drawing.Point(335, 62)
        Me.lblAltEmail.Name = "lblAltEmail"
        Me.lblAltEmail.ReadOnly = True
        Me.lblAltEmail.Size = New System.Drawing.Size(391, 16)
        Me.lblAltEmail.TabIndex = 57
        '
        'lblEmail
        '
        Me.lblEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(335, 33)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.ReadOnly = True
        Me.lblEmail.Size = New System.Drawing.Size(391, 16)
        Me.lblEmail.TabIndex = 56
        '
        'lblInterimName
        '
        Me.lblInterimName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblInterimName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterimName.Location = New System.Drawing.Point(142, 91)
        Me.lblInterimName.Name = "lblInterimName"
        Me.lblInterimName.ReadOnly = True
        Me.lblInterimName.Size = New System.Drawing.Size(174, 16)
        Me.lblInterimName.TabIndex = 55
        '
        'lblAltName
        '
        Me.lblAltName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblAltName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltName.Location = New System.Drawing.Point(142, 62)
        Me.lblAltName.Name = "lblAltName"
        Me.lblAltName.ReadOnly = True
        Me.lblAltName.Size = New System.Drawing.Size(174, 16)
        Me.lblAltName.TabIndex = 54
        '
        'lblName
        '
        Me.lblName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(142, 33)
        Me.lblName.Name = "lblName"
        Me.lblName.ReadOnly = True
        Me.lblName.Size = New System.Drawing.Size(174, 16)
        Me.lblName.TabIndex = 53
        '
        'btnShowIntCo
        '
        Me.btnShowIntCo.Location = New System.Drawing.Point(6, 86)
        Me.btnShowIntCo.Name = "btnShowIntCo"
        Me.btnShowIntCo.Size = New System.Drawing.Size(116, 23)
        Me.btnShowIntCo.TabIndex = 52
        Me.btnShowIntCo.Text = "Interim Contact"
        Me.btnShowIntCo.UseVisualStyleBackColor = True
        '
        'btnShowAltCo
        '
        Me.btnShowAltCo.Location = New System.Drawing.Point(6, 57)
        Me.btnShowAltCo.Name = "btnShowAltCo"
        Me.btnShowAltCo.Size = New System.Drawing.Size(116, 23)
        Me.btnShowAltCo.TabIndex = 51
        Me.btnShowAltCo.Text = "Alternate Contact"
        Me.btnShowAltCo.UseVisualStyleBackColor = True
        '
        'btnShow
        '
        Me.btnShow.Location = New System.Drawing.Point(6, 28)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(116, 23)
        Me.btnShow.TabIndex = 50
        Me.btnShow.Text = "Company Coordinator"
        Me.btnShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(766, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 49
        Me.Label6.Text = "Telephone"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(332, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Email Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(139, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Name"
        '
        'MainPanel
        '
        Me.MainPanel.BackColor = System.Drawing.Color.AliceBlue
        Me.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MainPanel.Controls.Add(Me.Panel1)
        Me.MainPanel.Controls.Add(Me.pnlRefinery)
        Me.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainPanel.Location = New System.Drawing.Point(3, 3)
        Me.MainPanel.Name = "MainPanel"
        Me.MainPanel.Size = New System.Drawing.Size(966, 452)
        Me.MainPanel.TabIndex = 20
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Controls.Add(Me.PCCEmail)
        Me.Panel1.Controls.Add(Me.lblPCCEmail)
        Me.Panel1.Controls.Add(Me.lblPCCName)
        Me.Panel1.Controls.Add(Me.label)
        Me.Panel1.Location = New System.Drawing.Point(7, 340)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(943, 103)
        Me.Panel1.TabIndex = 133
        '
        'PCCEmail
        '
        Me.PCCEmail.Image = CType(resources.GetObject("PCCEmail.Image"), System.Drawing.Image)
        Me.PCCEmail.Location = New System.Drawing.Point(737, 14)
        Me.PCCEmail.Name = "PCCEmail"
        Me.PCCEmail.Size = New System.Drawing.Size(28, 21)
        Me.PCCEmail.TabIndex = 136
        Me.PCCEmail.TabStop = False
        '
        'lblPCCEmail
        '
        Me.lblPCCEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPCCEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPCCEmail.Location = New System.Drawing.Point(327, 15)
        Me.lblPCCEmail.Name = "lblPCCEmail"
        Me.lblPCCEmail.ReadOnly = True
        Me.lblPCCEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblPCCEmail.TabIndex = 135
        '
        'lblPCCName
        '
        Me.lblPCCName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPCCName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPCCName.Location = New System.Drawing.Point(136, 15)
        Me.lblPCCName.Name = "lblPCCName"
        Me.lblPCCName.ReadOnly = True
        Me.lblPCCName.Size = New System.Drawing.Size(174, 16)
        Me.lblPCCName.TabIndex = 134
        '
        'label
        '
        Me.label.AutoSize = True
        Me.label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label.ForeColor = System.Drawing.Color.Black
        Me.label.Location = New System.Drawing.Point(5, 15)
        Me.label.Name = "label"
        Me.label.Size = New System.Drawing.Size(125, 13)
        Me.label.TabIndex = 133
        Me.label.Text = "Primary Client Coord."
        '
        'pnlRefinery
        '
        Me.pnlRefinery.BackColor = System.Drawing.Color.SkyBlue
        Me.pnlRefinery.Controls.Add(Me.Label28)
        Me.pnlRefinery.Controls.Add(Me.Label26)
        Me.pnlRefinery.Controls.Add(Me.lblPricingPhone)
        Me.pnlRefinery.Controls.Add(Me.PCEmail)
        Me.pnlRefinery.Controls.Add(Me.lblPricingContactEmail)
        Me.pnlRefinery.Controls.Add(Me.lblPricingContact)
        Me.pnlRefinery.Controls.Add(Me.Label20)
        Me.pnlRefinery.Controls.Add(Me.TMEmail)
        Me.pnlRefinery.Controls.Add(Me.MMEmail)
        Me.pnlRefinery.Controls.Add(Me.OMEmail)
        Me.pnlRefinery.Controls.Add(Me.RMEmail)
        Me.pnlRefinery.Controls.Add(Me.RACEmail)
        Me.pnlRefinery.Controls.Add(Me.RCEmail)
        Me.pnlRefinery.Controls.Add(Me.lblRefAltPhone)
        Me.pnlRefinery.Controls.Add(Me.lblRefCoPhone)
        Me.pnlRefinery.Controls.Add(Me.lblTechMgrEmail)
        Me.pnlRefinery.Controls.Add(Me.lblMaintMgrEmail)
        Me.pnlRefinery.Controls.Add(Me.lblOpsMgrEmail)
        Me.pnlRefinery.Controls.Add(Me.lblRefMgrEmail)
        Me.pnlRefinery.Controls.Add(Me.lblRefAltEmail)
        Me.pnlRefinery.Controls.Add(Me.lblRefCoEmail)
        Me.pnlRefinery.Controls.Add(Me.lblTechMgrName)
        Me.pnlRefinery.Controls.Add(Me.lblMaintMgrName)
        Me.pnlRefinery.Controls.Add(Me.lblOpsMgrName)
        Me.pnlRefinery.Controls.Add(Me.lblRefMgrName)
        Me.pnlRefinery.Controls.Add(Me.lblRefAltName)
        Me.pnlRefinery.Controls.Add(Me.lblRefCoName)
        Me.pnlRefinery.Controls.Add(Me.Label30)
        Me.pnlRefinery.Controls.Add(Me.Label27)
        Me.pnlRefinery.Controls.Add(Me.Label24)
        Me.pnlRefinery.Controls.Add(Me.Label23)
        Me.pnlRefinery.Location = New System.Drawing.Point(6, 135)
        Me.pnlRefinery.Name = "pnlRefinery"
        Me.pnlRefinery.Size = New System.Drawing.Size(943, 199)
        Me.pnlRefinery.TabIndex = 108
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.Black
        Me.Label28.Location = New System.Drawing.Point(6, 40)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(106, 13)
        Me.Label28.TabIndex = 139
        Me.Label28.Text = "Alternate Contact"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(6, 17)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(123, 13)
        Me.Label26.TabIndex = 138
        Me.Label26.Text = "Refinery Coordinator"
        '
        'lblPricingPhone
        '
        Me.lblPricingPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPricingPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPricingPhone.Location = New System.Drawing.Point(767, 60)
        Me.lblPricingPhone.Name = "lblPricingPhone"
        Me.lblPricingPhone.ReadOnly = True
        Me.lblPricingPhone.Size = New System.Drawing.Size(139, 16)
        Me.lblPricingPhone.TabIndex = 137
        '
        'PCEmail
        '
        Me.PCEmail.Image = CType(resources.GetObject("PCEmail.Image"), System.Drawing.Image)
        Me.PCEmail.Location = New System.Drawing.Point(738, 59)
        Me.PCEmail.Name = "PCEmail"
        Me.PCEmail.Size = New System.Drawing.Size(28, 21)
        Me.PCEmail.TabIndex = 136
        Me.PCEmail.TabStop = False
        '
        'lblPricingContactEmail
        '
        Me.lblPricingContactEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPricingContactEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPricingContactEmail.Location = New System.Drawing.Point(328, 62)
        Me.lblPricingContactEmail.Name = "lblPricingContactEmail"
        Me.lblPricingContactEmail.ReadOnly = True
        Me.lblPricingContactEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblPricingContactEmail.TabIndex = 135
        '
        'lblPricingContact
        '
        Me.lblPricingContact.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblPricingContact.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPricingContact.Location = New System.Drawing.Point(137, 62)
        Me.lblPricingContact.Name = "lblPricingContact"
        Me.lblPricingContact.ReadOnly = True
        Me.lblPricingContact.Size = New System.Drawing.Size(174, 16)
        Me.lblPricingContact.TabIndex = 134
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(6, 62)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(94, 13)
        Me.Label20.TabIndex = 133
        Me.Label20.Text = "Pricing Contact"
        '
        'TMEmail
        '
        Me.TMEmail.Image = CType(resources.GetObject("TMEmail.Image"), System.Drawing.Image)
        Me.TMEmail.Location = New System.Drawing.Point(738, 159)
        Me.TMEmail.Name = "TMEmail"
        Me.TMEmail.Size = New System.Drawing.Size(28, 21)
        Me.TMEmail.TabIndex = 132
        Me.TMEmail.TabStop = False
        '
        'MMEmail
        '
        Me.MMEmail.Image = CType(resources.GetObject("MMEmail.Image"), System.Drawing.Image)
        Me.MMEmail.Location = New System.Drawing.Point(738, 136)
        Me.MMEmail.Name = "MMEmail"
        Me.MMEmail.Size = New System.Drawing.Size(28, 21)
        Me.MMEmail.TabIndex = 131
        Me.MMEmail.TabStop = False
        '
        'OMEmail
        '
        Me.OMEmail.Image = CType(resources.GetObject("OMEmail.Image"), System.Drawing.Image)
        Me.OMEmail.Location = New System.Drawing.Point(738, 113)
        Me.OMEmail.Name = "OMEmail"
        Me.OMEmail.Size = New System.Drawing.Size(28, 21)
        Me.OMEmail.TabIndex = 130
        Me.OMEmail.TabStop = False
        '
        'RMEmail
        '
        Me.RMEmail.Image = CType(resources.GetObject("RMEmail.Image"), System.Drawing.Image)
        Me.RMEmail.Location = New System.Drawing.Point(738, 90)
        Me.RMEmail.Name = "RMEmail"
        Me.RMEmail.Size = New System.Drawing.Size(28, 21)
        Me.RMEmail.TabIndex = 129
        Me.RMEmail.TabStop = False
        '
        'RACEmail
        '
        Me.RACEmail.Image = CType(resources.GetObject("RACEmail.Image"), System.Drawing.Image)
        Me.RACEmail.Location = New System.Drawing.Point(738, 38)
        Me.RACEmail.Name = "RACEmail"
        Me.RACEmail.Size = New System.Drawing.Size(28, 21)
        Me.RACEmail.TabIndex = 128
        Me.RACEmail.TabStop = False
        '
        'RCEmail
        '
        Me.RCEmail.Image = CType(resources.GetObject("RCEmail.Image"), System.Drawing.Image)
        Me.RCEmail.Location = New System.Drawing.Point(738, 11)
        Me.RCEmail.Name = "RCEmail"
        Me.RCEmail.Size = New System.Drawing.Size(28, 21)
        Me.RCEmail.TabIndex = 127
        Me.RCEmail.TabStop = False
        '
        'lblRefAltPhone
        '
        Me.lblRefAltPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefAltPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefAltPhone.Location = New System.Drawing.Point(765, 37)
        Me.lblRefAltPhone.Name = "lblRefAltPhone"
        Me.lblRefAltPhone.ReadOnly = True
        Me.lblRefAltPhone.Size = New System.Drawing.Size(141, 16)
        Me.lblRefAltPhone.TabIndex = 126
        '
        'lblRefCoPhone
        '
        Me.lblRefCoPhone.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefCoPhone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefCoPhone.Location = New System.Drawing.Point(765, 13)
        Me.lblRefCoPhone.Name = "lblRefCoPhone"
        Me.lblRefCoPhone.ReadOnly = True
        Me.lblRefCoPhone.Size = New System.Drawing.Size(141, 16)
        Me.lblRefCoPhone.TabIndex = 125
        '
        'lblTechMgrEmail
        '
        Me.lblTechMgrEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblTechMgrEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTechMgrEmail.Location = New System.Drawing.Point(328, 162)
        Me.lblTechMgrEmail.Name = "lblTechMgrEmail"
        Me.lblTechMgrEmail.ReadOnly = True
        Me.lblTechMgrEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblTechMgrEmail.TabIndex = 124
        '
        'lblMaintMgrEmail
        '
        Me.lblMaintMgrEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblMaintMgrEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaintMgrEmail.Location = New System.Drawing.Point(328, 139)
        Me.lblMaintMgrEmail.Name = "lblMaintMgrEmail"
        Me.lblMaintMgrEmail.ReadOnly = True
        Me.lblMaintMgrEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblMaintMgrEmail.TabIndex = 123
        '
        'lblOpsMgrEmail
        '
        Me.lblOpsMgrEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblOpsMgrEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpsMgrEmail.Location = New System.Drawing.Point(328, 116)
        Me.lblOpsMgrEmail.Name = "lblOpsMgrEmail"
        Me.lblOpsMgrEmail.ReadOnly = True
        Me.lblOpsMgrEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblOpsMgrEmail.TabIndex = 122
        '
        'lblRefMgrEmail
        '
        Me.lblRefMgrEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefMgrEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefMgrEmail.Location = New System.Drawing.Point(328, 93)
        Me.lblRefMgrEmail.Name = "lblRefMgrEmail"
        Me.lblRefMgrEmail.ReadOnly = True
        Me.lblRefMgrEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblRefMgrEmail.TabIndex = 121
        '
        'lblRefAltEmail
        '
        Me.lblRefAltEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefAltEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefAltEmail.Location = New System.Drawing.Point(328, 38)
        Me.lblRefAltEmail.Name = "lblRefAltEmail"
        Me.lblRefAltEmail.ReadOnly = True
        Me.lblRefAltEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblRefAltEmail.TabIndex = 120
        '
        'lblRefCoEmail
        '
        Me.lblRefCoEmail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefCoEmail.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefCoEmail.Location = New System.Drawing.Point(328, 14)
        Me.lblRefCoEmail.Name = "lblRefCoEmail"
        Me.lblRefCoEmail.ReadOnly = True
        Me.lblRefCoEmail.Size = New System.Drawing.Size(396, 16)
        Me.lblRefCoEmail.TabIndex = 119
        '
        'lblTechMgrName
        '
        Me.lblTechMgrName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblTechMgrName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTechMgrName.Location = New System.Drawing.Point(137, 162)
        Me.lblTechMgrName.Name = "lblTechMgrName"
        Me.lblTechMgrName.ReadOnly = True
        Me.lblTechMgrName.Size = New System.Drawing.Size(174, 16)
        Me.lblTechMgrName.TabIndex = 118
        '
        'lblMaintMgrName
        '
        Me.lblMaintMgrName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblMaintMgrName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaintMgrName.Location = New System.Drawing.Point(137, 139)
        Me.lblMaintMgrName.Name = "lblMaintMgrName"
        Me.lblMaintMgrName.ReadOnly = True
        Me.lblMaintMgrName.Size = New System.Drawing.Size(174, 16)
        Me.lblMaintMgrName.TabIndex = 117
        '
        'lblOpsMgrName
        '
        Me.lblOpsMgrName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblOpsMgrName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpsMgrName.Location = New System.Drawing.Point(137, 116)
        Me.lblOpsMgrName.Name = "lblOpsMgrName"
        Me.lblOpsMgrName.ReadOnly = True
        Me.lblOpsMgrName.Size = New System.Drawing.Size(174, 16)
        Me.lblOpsMgrName.TabIndex = 116
        '
        'lblRefMgrName
        '
        Me.lblRefMgrName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefMgrName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefMgrName.Location = New System.Drawing.Point(137, 93)
        Me.lblRefMgrName.Name = "lblRefMgrName"
        Me.lblRefMgrName.ReadOnly = True
        Me.lblRefMgrName.Size = New System.Drawing.Size(174, 16)
        Me.lblRefMgrName.TabIndex = 115
        '
        'lblRefAltName
        '
        Me.lblRefAltName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefAltName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefAltName.Location = New System.Drawing.Point(137, 38)
        Me.lblRefAltName.Name = "lblRefAltName"
        Me.lblRefAltName.ReadOnly = True
        Me.lblRefAltName.Size = New System.Drawing.Size(174, 16)
        Me.lblRefAltName.TabIndex = 114
        '
        'lblRefCoName
        '
        Me.lblRefCoName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblRefCoName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefCoName.Location = New System.Drawing.Point(137, 14)
        Me.lblRefCoName.Name = "lblRefCoName"
        Me.lblRefCoName.ReadOnly = True
        Me.lblRefCoName.Size = New System.Drawing.Size(174, 16)
        Me.lblRefCoName.TabIndex = 113
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.Black
        Me.Label30.Location = New System.Drawing.Point(6, 162)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(116, 13)
        Me.Label30.TabIndex = 112
        Me.Label30.Text = "Technical Manager"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(6, 139)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(133, 13)
        Me.Label27.TabIndex = 111
        Me.Label27.Text = "Maintenance Manager"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(6, 116)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(121, 13)
        Me.Label24.TabIndex = 110
        Me.Label24.Text = "Operations Manager"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(6, 93)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(107, 13)
        Me.Label23.TabIndex = 109
        Me.Label23.Text = "Refinery Manager"
        '
        'ConsoleTabs
        '
        Me.ConsoleTabs.AllowDrop = True
        Me.ConsoleTabs.CausesValidation = False
        Me.ConsoleTabs.Controls.Add(Me.tabContacts)
        Me.ConsoleTabs.Controls.Add(Me.tabCorr)
        Me.ConsoleTabs.Controls.Add(Me.tabCheckList)
        Me.ConsoleTabs.Controls.Add(Me.tabNotes)
        Me.ConsoleTabs.Controls.Add(Me.tabCI)
        Me.ConsoleTabs.Controls.Add(Me.tabSS)
        Me.ConsoleTabs.Controls.Add(Me.tabTimeGrade)
        Me.ConsoleTabs.Controls.Add(Me.tabDD)
        Me.ConsoleTabs.Controls.Add(Me.tabClippy)
        Me.ConsoleTabs.Controls.Add(Me.tabQA)
        Me.ConsoleTabs.Enabled = False
        Me.ConsoleTabs.Location = New System.Drawing.Point(12, 211)
        Me.ConsoleTabs.Name = "ConsoleTabs"
        Me.ConsoleTabs.SelectedIndex = 0
        Me.ConsoleTabs.Size = New System.Drawing.Size(980, 484)
        Me.ConsoleTabs.TabIndex = 0
        '
        'MainConsole
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.ClientSize = New System.Drawing.Size(1004, 726)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.txtVersion)
        Me.Controls.Add(Me.ConsoleTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "MainConsole"
        Me.Text = "Refining Validation Console"
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.tabQA.ResumeLayout(False)
        Me.tabQA.PerformLayout()
        Me.tabClippy.ResumeLayout(False)
        Me.tabClippy.PerformLayout()
        CType(Me.dgClippyResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDD.ResumeLayout(False)
        Me.tabDD.PerformLayout()
        CType(Me.dgFiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabTimeGrade.ResumeLayout(False)
        Me.tabTimeGrade.PerformLayout()
        CType(Me.dgHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSS.ResumeLayout(False)
        Me.tabSS.PerformLayout()
        Me.tabCI.ResumeLayout(False)
        Me.tabCI.PerformLayout()
        CType(Me.dgContinuingIssues, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabNotes.ResumeLayout(False)
        Me.tabNotes.PerformLayout()
        Me.tabCheckList.ResumeLayout(False)
        Me.tabCheckList.PerformLayout()
        Me.tabCorr.ResumeLayout(False)
        Me.tabCorr.PerformLayout()
        Me.tabContacts.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.pnlCompany.ResumeLayout(False)
        Me.pnlCompany.PerformLayout()
        CType(Me.ICEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACEmail2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CCEmail1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainPanel.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PCCEmail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlRefinery.ResumeLayout(False)
        Me.pnlRefinery.PerformLayout()
        CType(Me.PCEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TMEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MMEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OMEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RMEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RACEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RCEmail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConsoleTabs.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents txtVersion As System.Windows.Forms.TextBox
    Friend WithEvents VersionToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents btnKillProcesses As System.Windows.Forms.Button
    Friend WithEvents btnSendPrelimPricing As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents btnCreateMissingPNFiles As System.Windows.Forms.Button
    Friend WithEvents btnBug As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblItemsRemaining As System.Windows.Forms.Label
    Friend WithEvents lblBlueFlags As System.Windows.Forms.Label
    Friend WithEvents lblRedFlags As System.Windows.Forms.Label
    Friend WithEvents lblLastCalcDate As System.Windows.Forms.Label
    Friend WithEvents lblLastUpload As System.Windows.Forms.Label
    Friend WithEvents lblLastFileSave As System.Windows.Forms.Label
    Friend WithEvents lblValidationStatus As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnSS As System.Windows.Forms.Button
    Friend WithEvents btnValidate As System.Windows.Forms.Button
    Friend WithEvents btnJustLooking As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents btnFuelLube As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboConsultant As System.Windows.Forms.ComboBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents cboRefNum As System.Windows.Forms.ComboBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents cboStudy As System.Windows.Forms.ComboBox
    Friend WithEvents btnMain As System.Windows.Forms.Button
    Friend WithEvents btnCompanyPW As System.Windows.Forms.Button
    Friend WithEvents btnReturnPW As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btnValFax As System.Windows.Forms.Button
    Friend WithEvents Help As System.Windows.Forms.HelpProvider
    Friend WithEvents btnFLValidation As System.Windows.Forms.Button
    Friend WithEvents tabQA As System.Windows.Forms.TabPage
    Friend WithEvents QASearch As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lnkResultsPresentation As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPricing As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMisc As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkProcessData As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkOpex As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMaintenance As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPersonnel As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkEnergy As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMaterialBalance As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkStudyBoundary As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkRefineryHistory As System.Windows.Forms.LinkLabel
    Friend WithEvents tabClippy As System.Windows.Forms.TabPage
    Friend WithEvents btnQueryQuit As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents chkSQL As System.Windows.Forms.CheckBox
    Friend WithEvents dgClippyResults As System.Windows.Forms.DataGridView
    Friend WithEvents btnClippySearch As System.Windows.Forms.Button
    Friend WithEvents txtClippySearch As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents tabDD As System.Windows.Forms.TabPage
    Friend WithEvents optCompanyCorr As System.Windows.Forms.CheckBox
    Friend WithEvents txtMessageFilename As System.Windows.Forms.TextBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents btnGVClear As System.Windows.Forms.Button
    Friend WithEvents btnFilesSave As System.Windows.Forms.Button
    Friend WithEvents dgFiles As System.Windows.Forms.DataGridView
    Friend WithEvents OriginalFilename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Filenames As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FileDestination As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents tabTimeGrade As System.Windows.Forms.TabPage
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgHistory As System.Windows.Forms.DataGridView
    Friend WithEvents dgSummary As System.Windows.Forms.DataGridView
    Friend WithEvents btnGradeExit As System.Windows.Forms.Button
    Friend WithEvents btnSection As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgGrade As System.Windows.Forms.DataGridView
    Friend WithEvents tabSS As System.Windows.Forms.TabPage
    Friend WithEvents btnDirRefresh As System.Windows.Forms.Button
    Friend WithEvents btnSecureSend As System.Windows.Forms.Button
    Friend WithEvents tvwCompCorr2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwDrawings2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwClientAttachments2 As System.Windows.Forms.TreeView
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboDir2 As System.Windows.Forms.ComboBox
    Friend WithEvents tvwClientAttachments As System.Windows.Forms.TreeView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDir As System.Windows.Forms.ComboBox
    Friend WithEvents tvwDrawings As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence As System.Windows.Forms.TreeView
    Friend WithEvents tabCI As System.Windows.Forms.TabPage
    Friend WithEvents chkNoDates As System.Windows.Forms.CheckBox
    Friend WithEvents btnExportCI As System.Windows.Forms.Button
    Friend WithEvents btnNewIssue As System.Windows.Forms.Button
    Friend WithEvents txtCI As System.Windows.Forms.TextBox
    Friend WithEvents btnSaveCI As System.Windows.Forms.Button
    Friend WithEvents dgContinuingIssues As System.Windows.Forms.DataGridView
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EntryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Issue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EditDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DeletedBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DeletedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Deleted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tabNotes As System.Windows.Forms.TabPage
    Friend WithEvents btnUnlockPN As System.Windows.Forms.Button
    Friend WithEvents btnCreatePN As System.Windows.Forms.Button
    Friend WithEvents btnSavePN As System.Windows.Forms.Button
    Friend WithEvents txtNotes As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents tabCheckList As System.Windows.Forms.TabPage
    Friend WithEvents tvIssues As System.Windows.Forms.TreeView
    Friend WithEvents txtIssueName As System.Windows.Forms.TextBox
    Friend WithEvents txtIssueID As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateIssue As System.Windows.Forms.Button
    Friend WithEvents lblItemCount As System.Windows.Forms.Label
    Friend WithEvents btnAddIssue As System.Windows.Forms.Button
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents ValCheckList As System.Windows.Forms.CheckedListBox
    Friend WithEvents tabCorr As System.Windows.Forms.TabPage
    Friend WithEvents btnPolishRpt As System.Windows.Forms.Button
    Friend WithEvents btnRAM As System.Windows.Forms.Button
    Friend WithEvents btnVI As System.Windows.Forms.Button
    Friend WithEvents btnSC As System.Windows.Forms.Button
    Friend WithEvents btnPA As System.Windows.Forms.Button
    Friend WithEvents btnCT As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnDrawings As System.Windows.Forms.Button
    Friend WithEvents btnHYC As System.Windows.Forms.Button
    Friend WithEvents btnPT As System.Windows.Forms.Button
    Friend WithEvents btnSpecFrac As System.Windows.Forms.Button
    Friend WithEvents btnUnitReview As System.Windows.Forms.Button
    Friend WithEvents btnBuildVRFile As System.Windows.Forms.Button
    Friend WithEvents btnReceiptAck As System.Windows.Forms.Button
    Friend WithEvents lstReturnFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents lstVRFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents lstVFFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents lstVFNumbers As System.Windows.Forms.ListBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents tabContacts As System.Windows.Forms.TabPage
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlCompany As System.Windows.Forms.Panel
    Friend WithEvents ICEmail As System.Windows.Forms.PictureBox
    Friend WithEvents ACEmail2 As System.Windows.Forms.PictureBox
    Friend WithEvents CCEmail1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblInterimPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblAltPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblInterimEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblAltEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblInterimName As System.Windows.Forms.TextBox
    Friend WithEvents lblAltName As System.Windows.Forms.TextBox
    Friend WithEvents lblName As System.Windows.Forms.TextBox
    Friend WithEvents btnShowIntCo As System.Windows.Forms.Button
    Friend WithEvents btnShowAltCo As System.Windows.Forms.Button
    Friend WithEvents btnShow As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents MainPanel As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PCCEmail As System.Windows.Forms.PictureBox
    Friend WithEvents lblPCCEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblPCCName As System.Windows.Forms.TextBox
    Friend WithEvents label As System.Windows.Forms.Label
    Friend WithEvents pnlRefinery As System.Windows.Forms.Panel
    Friend WithEvents lblPricingPhone As System.Windows.Forms.TextBox
    Friend WithEvents PCEmail As System.Windows.Forms.PictureBox
    Friend WithEvents lblPricingContactEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblPricingContact As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TMEmail As System.Windows.Forms.PictureBox
    Friend WithEvents MMEmail As System.Windows.Forms.PictureBox
    Friend WithEvents OMEmail As System.Windows.Forms.PictureBox
    Friend WithEvents RMEmail As System.Windows.Forms.PictureBox
    Friend WithEvents RACEmail As System.Windows.Forms.PictureBox
    Friend WithEvents RCEmail As System.Windows.Forms.PictureBox
    Friend WithEvents lblRefAltPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblRefCoPhone As System.Windows.Forms.TextBox
    Friend WithEvents lblTechMgrEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblMaintMgrEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblOpsMgrEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblRefMgrEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblRefAltEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblRefCoEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblTechMgrName As System.Windows.Forms.TextBox
    Friend WithEvents lblMaintMgrName As System.Windows.Forms.TextBox
    Friend WithEvents lblOpsMgrName As System.Windows.Forms.TextBox
    Friend WithEvents lblRefMgrName As System.Windows.Forms.TextBox
    Friend WithEvents lblRefAltName As System.Windows.Forms.TextBox
    Friend WithEvents lblRefCoName As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ConsoleTabs As System.Windows.Forms.TabControl
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents chkIDR As System.Windows.Forms.CheckBox
    Friend WithEvents lblCombo As System.Windows.Forms.Label
    Friend WithEvents btnIssueCancel As System.Windows.Forms.Button
    Friend WithEvents btnDDExit As System.Windows.Forms.Button
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents chkUseRefnum As System.Windows.Forms.CheckBox
    Friend WithEvents tv As System.Windows.Forms.TreeView


End Class
