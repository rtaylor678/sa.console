﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.Data.SqlClient
Imports DownloadManagerLibrary
Imports System.Runtime.InteropServices
Imports System.Collections.Generic
Imports SA.Internal.Console.DataObject
Imports iwantedue.Windows.Forms




Public Class SecureSend

    Public SendIDR As Boolean
    Dim ds As DataSet
    Dim db As DataObject
    Dim DLMLink As String
    Dim FileList As String
    Dim ValidationLinks() As String
    Dim chkFiles(10) As System.Windows.Forms.CheckBox
    Dim fdlg As FileDialog = Nothing
    Dim Emails As New List(Of String)
    Dim RefineryEmail As String
    Dim AltRefineryEmail As String
    Dim CompanyEmail As String
    Public CompanyContactName As String
    Dim AltCompanyEmail As String
    Dim EmailAddress As String
    Dim PricingEmail As String
    Dim PlantEmail As String
    Dim DCEmail As String
    Dim Coloc As String = ""
    Dim CorrTree As TreeView
    Dim CompanyTree As TreeView
    Dim DrawingTree As TreeView
    Dim CATree As TreeView
    Dim CompCorrTree As TreeView
    Dim PolishCorrTree As TreeView
    Dim PolishCorrTree2 As TreeView
    Dim CorrTree2 As TreeView
    Dim CompanyTree2 As TreeView
    Dim DrawingTree2 As TreeView
    Dim CATree2 As TreeView
    Dim CompCorrTree2 As TreeView
    Private Err As ErrorLog
    Private mPassword As String
    Dim lstFoundNode As List(Of String)
    Public PCCEmail As String
    Public StudyType As String
    Public IDRFile As String
    Dim strFaxName As String
    Dim strFaxEmail As String
    Dim subj As String = ""
    Dim ConsultantName As String
    Dim FirstName As String = ""

    Private mCompany As String
    Public Property Company() As String
        Get
            Return mCompany
        End Get
        Set(ByVal value As String)
            mCompany = value
        End Set
    End Property

    Private mCompanyPassword As String
    Public Property CompanyPassword() As String
        Get
            Return mCompanyPassword
        End Get
        Set(ByVal value As String)
            mCompanyPassword = value
        End Set
    End Property
    Private mLoc As String
    Public Property Loc() As String
        Get
            Return mLoc
        End Get
        Set(ByVal value As String)
            mLoc = value
        End Set
    End Property

    Private mUser As String
    Public Property User() As String
        Get
            Return mUser
        End Get
        Set(ByVal value As String)
            mUser = value
        End Set
    End Property

    Private mRefNum As String
    Public Property RefNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property
    Private mLubeRefNum As String
    Public Property LubeRefNum() As String
        Get
            Return mLubeRefNum
        End Get
        Set(ByVal value As String)
            mLubeRefNum = value
        End Set
    End Property

    Private mStudy As String
    Public Property Study() As String
        Get
            Return mStudy
        End Get
        Set(ByVal value As String)
            mStudy = value
        End Set
    End Property
    Private mPolishCorrPath As String
    Public Property PolishCorrPath() As String
        Get
            Return mPolishCorrPath
        End Get
        Set(ByVal value As String)
            mPolishCorrPath = value
        End Set
    End Property

    Private mPolishCorrPath2 As String
    Public Property PolishCorrPath2() As String
        Get
            Return mPolishCorrPath2
        End Get
        Set(ByVal value As String)
            mPolishCorrPath2 = value
        End Set
    End Property

    Private mCorrPath As String
    Public Property CorrPath() As String
        Get
            Return mCorrPath
        End Get
        Set(ByVal value As String)
            mCorrPath = value
        End Set
    End Property

    Private mCompanyPath As String
    Public Property CompanyPath() As String
        Get
            Return mCompanyPath
        End Get
        Set(ByVal value As String)
            mCompanyPath = value
        End Set
    End Property

    Private mDrawingPath As String
    Public Property DrawingPath() As String
        Get
            Return mDrawingPath
        End Get
        Set(ByVal value As String)
            mDrawingPath = value
        End Set
    End Property
    Private mCorrPath2 As String
    Public Property CorrPath2() As String
        Get
            Return mCorrPath2
        End Get
        Set(ByVal value As String)
            mCorrPath2 = value
        End Set
    End Property

    Private mCompanyPath2 As String
    Public Property CompanyPath2() As String
        Get
            Return mCompanyPath2
        End Get
        Set(ByVal value As String)
            mCompanyPath2 = value
        End Set
    End Property

    Private mDrawingPath2 As String
    Public Property DrawingPath2() As String
        Get
            Return mDrawingPath2
        End Get
        Set(ByVal value As String)
            mDrawingPath2 = value
        End Set
    End Property
    Private mTempPath As String
    Public Property TempPath() As String
        Get
            Return mTempPath
        End Get
        Set(ByVal value As String)
            mTempPath = value
        End Set
    End Property

    Private mTemplatePath As String
    Public Property TemplatePath() As String
        Get
            Return mTemplatePath
        End Get
        Set(ByVal value As String)
            mTemplatePath = value
        End Set
    End Property

    Private mClientAttachmentsPath As String
    Public Property ClientAttachmentsPath() As String
        Get
            Return mClientAttachmentsPath
        End Get
        Set(ByVal value As String)
            mClientAttachmentsPath = value
        End Set
    End Property
    Private mClientAttachmentsPath2 As String
    Public Property ClientAttachmentsPath2() As String
        Get
            Return mClientAttachmentsPath2
        End Get
        Set(ByVal value As String)
            mClientAttachmentsPath2 = value
        End Set
    End Property
    Private mCompCorrPath As String
    Public Property CompCorrPath() As String
        Get
            Return mCompCorrPath
        End Get
        Set(ByVal value As String)
            mCompCorrPath = value
        End Set
    End Property
    Private mCompCorrPath2 As String
    Public Property CompCorrPath2() As String
        Get
            Return mCompCorrPath2
        End Get
        Set(ByVal value As String)
            mCompCorrPath2 = value
        End Set
    End Property

    Private mSendMethod As String
    Public Property SendMethod() As String
        Get
            Return mSendMethod
        End Get
        Set(ByVal value As String)
            mSendMethod = value
        End Set
    End Property
    Private mSendTo As String
    Public Property SendTo() As String
        Get
            Return mSendTo
        End Get
        Set(ByVal value As String)
            mSendTo = value
        End Set
    End Property
    Private mSendCC As String
    Public Property SendCC() As String
        Get
            Return mSendCC
        End Get
        Set(ByVal value As String)
            mSendCC = value
        End Set
    End Property


    Public Sub Clear()
        Loc = ""
        User = ""
        RefNum = ""
        Study = ""
        CorrPath = ""
        CompanyPath = ""
        DrawingPath = ""
        CorrPath2 = ""
        CompanyPath2 = ""
        DrawingPath2 = ""
        TempPath = ""
        TemplatePath = ""
        ClientAttachmentsPath = ""
        ClientAttachmentsPath2 = ""
        CompCorrPath = ""
        CompCorrPath2 = ""
        SendMethod = ""
        PolishCorrPath = ""
        PolishCorrPath2 = ""

    End Sub

    Public Sub New(StudyType As String, tvCorr As TreeView, tvDraw As TreeView, tvCA As TreeView, tvCompCorr As TreeView, tvCorr2 As TreeView, tvDraw2 As TreeView, tvCA2 As TreeView, tvCompCorr2 As TreeView, Optional tvPolishCorr As TreeView = Nothing, Optional tvPolishCorr2 As TreeView = Nothing, Optional username As String = Nothing, Optional password As String = Nothing)

        ' This call is required by the designer.
        InitializeComponent()
        Select Case StudyType.ToUpper
            Case "REFINING"
                db = New DataObject(DataObject.StudyTypes.REFINING, username, password)
            Case "OLEFINS"
                db = New DataObject(DataObject.StudyTypes.OLEFINS, username, password)
            Case "POWER"
                db = New DataObject(DataObject.StudyTypes.POWER, username, password)
            Case "RAM"
                db = New DataObject(DataObject.StudyTypes.RAM, username, password)
            Case "PIPELINES", "TERMINALS"
                db = New DataObject(DataObject.StudyTypes.PIPELINES, username, password)
        End Select
        ' Add any initialization after the InitializeComponent() call.

        CompCorrTree2 = tvCompCorr2
        CorrTree = tvCorr
        DrawingTree = tvDraw
        CATree = tvCA
        CompCorrTree = tvCompCorr
        CorrTree2 = tvCorr
        DrawingTree2 = tvDraw2
        CATree2 = tvCA2
        CompCorrTree2 = tvCompCorr2
        PolishCorrTree = tvPolishCorr
        PolishCorrTree2 = tvPolishCorr2
        Err = New ErrorLog(StudyType, username, password)
        Coloc = mLoc



    End Sub


    Private Function GetCompanyPassword(mRefNum As String) As String
        Dim pw As String = ""

        Dim params = New List(Of String)
        params.Add("RefNum/" + mRefNum)
        ds = db.ExecuteStoredProc("Console." & "GetCompanyPassword", params)

        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then

            pw = ds.Tables(0).Rows(0)("CompanyPassword").ToString.Trim()

        End If

        Return pw
    End Function
    Private Sub SecureSend_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Dim Interim As Boolean = False
        Dim CompCorrFiles As List(Of String)
        Dim PolishCorrFiles2 As List(Of String) = Nothing
        Dim PolishCorrFiles As List(Of String) = Nothing
        Dim CorrFiles As List(Of String)
        Dim DrawingFiles As List(Of String)
        Dim CAFiles As List(Of String)
        Dim CompCorrFiles2 As List(Of String)
        Dim CorrFiles2 As List(Of String)
        Dim DrawingFiles2 As List(Of String)
        Dim CAFiles2 As List(Of String)
        Dim file As String

        Try


            Utilities.DeleteFiles(TempPath)
            lstFilesToSend.Items.Clear()
            If IDRFile.Length > 1 Then lstFilesToSend.Items.Add(IDRFile)
            CompCorrFiles = TraverseTree(CompCorrTree, mCompCorrPath)
            If Not PolishCorrTree Is Nothing Then
                PolishCorrFiles = TraverseTree(PolishCorrTree, mPolishCorrPath)
                PolishCorrFiles2 = TraverseTree(PolishCorrTree2, mPolishCorrPath2)
            End If
            CorrFiles = TraverseTree(CorrTree, mCorrPath)
            DrawingFiles = TraverseTree(DrawingTree, mDrawingPath)
            CAFiles = TraverseTree(CATree, mClientAttachmentsPath)
            CompCorrFiles2 = TraverseTree(CompCorrTree2, mCompCorrPath)
            CorrFiles2 = TraverseTree(CorrTree2, mCorrPath)
            DrawingFiles2 = TraverseTree(DrawingTree2, mDrawingPath)
            CAFiles2 = TraverseTree(CATree2, mClientAttachmentsPath)


            For Each f In CompCorrFiles

                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    FileCopy(f, file)
                    lstFilesToSend.Items.Add(file)
                End If

            Next

            For Each f In CorrFiles
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            If Not PolishCorrTree Is Nothing Then
                For Each f In PolishCorrFiles
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next

                For Each f In PolishCorrFiles2
                    Utilities.GetLinkPath(f)
                    file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                    If lstFilesToSend.FindString(file) = -1 Then
                        If lstFilesToSend.FindString(file) = -1 Then
                            FileCopy(f, file)
                            lstFilesToSend.Items.Add(file)
                        End If
                    End If
                Next
            End If
            For Each f In DrawingFiles
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In CAFiles
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In CompCorrFiles2
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In CorrFiles2
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In DrawingFiles2
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next

            For Each f In CAFiles2
                Utilities.GetLinkPath(f)
                file = TempPath & f.Substring(f.LastIndexOf("\") + 1, f.Length - f.LastIndexOf("\") - 1)
                If lstFilesToSend.FindString(file) = -1 Then
                    If lstFilesToSend.FindString(file) = -1 Then
                        FileCopy(f, file)
                        lstFilesToSend.Items.Add(file)
                    End If
                End If
            Next



            If Study = "RAM" Then
                txtPassword.Enabled = False
                Label5.Visible = False
            Else
                txtPassword.Enabled = True
                Label5.Visible = True
            End If

            txtResponseDays.Text = "10"
            Me.cboSendMethod.Items.Clear()
            Me.cboSendMethod.Items.Add("DLM - Download Manager")
            Me.cboSendMethod.Items.Add("PPZ - Encrypted Zip File")
            Me.cboSendMethod.Items.Add("PPF - Encrypt Each File")

            cboSendMethod.SelectedText = "PPF - Encrypt Each File"
            mSendMethod = "PPF - Encrypt Each File"
            Dim params = New List(Of String)

            If Study = "POWER" Then
                cboTemplate.Enabled = False
                cboSendMethod.Enabled = False
                params.Clear()
                params.Add("RefNum/" + Utilities.GetSite(mRefNum, 1))
                params.Add("CompanyID/" + GetCompanyName(mRefNum))

                ds = db.ExecuteStoredProc("Console.GetContactEmails", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        txtEmail.Text = ds.Tables(0).Rows(0)(0).ToString.ToUpper.Trim
                        txtCC.Text = ds.Tables(0).Rows(0)(1).ToString.ToUpper.Trim

                    End If
                End If

                txtPassword.Text = GetCompanyPassword(mRefNum)

            Else

                params.Clear()
                params.Add("RefNum/" + mRefNum)
                Dim nRow As DataRow
                Dim AddtoEmails As Boolean
                ds = db.ExecuteStoredProc("Console.GetContactEmails", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then Emails.Add(ds.Tables(0).Rows(0)("EmailType").ToString.ToUpper + " - " + ds.Tables(0).Rows(0)("Email").ToString.ToUpper)
                    For i = 1 To ds.Tables(0).Rows.Count - 1
                        nRow = ds.Tables(0).Rows(i)
                        AddtoEmails = True
                        For Each mail In Emails
                            If mail.Contains(nRow("Email").ToString.ToUpper) Then AddtoEmails = False
                        Next
                        If AddtoEmails Then Emails.Add(nRow("EmailType").ToString.ToUpper + " - " + nRow("Email").ToString.ToUpper)
                    Next

                    If Study = "RAM" Then
                        txtPassword.Text = GetCompanyPassword(Company)
                    Else
                        txtPassword.Text = GetCompanyPassword(RefNum)
                    End If

                    If Emails.Count > 0 Then
                        For Each em In Emails

                            If em.ToUpper.Contains("ALT PIPELINE") And em.Contains("@") Then
                                AltRefineryEmail += StripEmail(em) & ";"
                            Else
                                If em.ToUpper.Contains("PIPELINE") And em.Contains("@") Then
                                    RefineryEmail += StripEmail(em) & ";"
                                End If
                            End If

                            If em.ToUpper.Contains("ALT REFINERY") And em.Contains("@") Then
                                AltRefineryEmail += StripEmail(em) & ";"
                            Else
                                If (em.ToUpper.Contains("REFINERY") Or em.ToUpper.Contains("SITE")) And em.Contains("@") Then
                                    RefineryEmail += StripEmail(em) & ";"
                                End If
                            End If
                            If em.ToUpper.Contains("ALT COMPANY") And em.Contains("@") Then
                                AltCompanyEmail += StripEmail(em) & ";"
                            Else

                                If em.ToUpper.Contains("COMPANY") And em.Contains("@") And Not Interim Then
                                    CompanyEmail += StripEmail(em) & ";"
                                End If


                            End If

                            If em.ToUpper.Contains("PLANT") And em.Contains("@") Then
                                PlantEmail += StripEmail(em) & ";"
                            End If
                            If em.ToUpper.Contains("PRICING") And em.Contains("@") Then
                                PricingEmail += StripEmail(em) & ";"
                            End If
                            If em.ToUpper.Contains("DC") And em.Contains("@") Then
                                DCEmail += StripEmail(em) & ";"
                            End If


                        Next


                        If Study.ToUpper = "REFINING" Or Study.ToUpper = "RAM" Or Study.ToUpper = "PIPELINES" Or Study.ToUpper = "TERMINALS" Then


                            If RefineryEmail Is Nothing Then
                                txtEmail.Text = IIf(CompanyEmail = Nothing, "", CompanyEmail)
                                txtCC.Text = IIf(AltCompanyEmail = Nothing, "", AltCompanyEmail) & IIf(AltRefineryEmail = Nothing, "", AltRefineryEmail)
                            Else
                                txtEmail.Text = IIf(RefineryEmail = Nothing, "", RefineryEmail)
                                txtCC.Text = IIf(CompanyEmail = Nothing, "", CompanyEmail) & IIf(AltCompanyEmail = Nothing, "", AltCompanyEmail) & IIf(AltRefineryEmail = Nothing, "", AltRefineryEmail)
                            End If

                            If RefineryEmail = "" And AltRefineryEmail = "" And CompanyEmail = "" Then
                                MessageBox.Show("No Contact Email Found")
                                Me.Close()
                            End If
                        Else

                            txtEmail.Text = PlantEmail
                            txtCC.Text = PricingEmail + DCEmail + CompanyEmail
                        End If
                    End If
                End If
            End If
            GetTemplates(mSendMethod)




        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
            Err.WriteLog("SecureSend Load", ex)
            CloseSS()
        End Try


    End Sub
    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function
    Private Function GetCompanyName(str As String) As String
        Dim CompanyName As String
        ''Get Company Name
        If str = "" Then Return ""
        Dim FirstDash As Integer = str.IndexOf("-")
        Dim SecondDash As Integer = str.IndexOf("-", FirstDash + 1)

        CompanyName = str.Substring(0, FirstDash - 1)

        Return CompanyName

    End Function


    Private Sub GetTemplates(sendMethod As String)
        Try
            cboTemplate.Items.Clear()

            If sendMethod = "DLM - Download Manager" Then
                Dim di As New IO.DirectoryInfo(TemplatePath)
                Dim diar1 As IO.FileInfo() = di.GetFiles("*DLM.txt")
                Dim dra As IO.FileInfo

                'list the names of all files in the specified directory
                For Each dra In diar1
                    cboTemplate.Items.Add(dra.ToString)
                Next
            Else
                Dim di As New IO.DirectoryInfo(TemplatePath)
                Dim diar1 As IO.FileInfo() = di.GetFiles("*.txt")
                Dim dra As IO.FileInfo

                'list the names of all files in the specified directory
                For Each dra In diar1
                    If Not dra.ToString.Contains("DLM") And Not dra.ToString.Contains("Notification of Transmittal") Then
                        cboTemplate.Items.Add(dra.ToString)
                    End If
                Next
            End If
            If Utilities.GetRefNumPart(RefNum, 2) = "LUB" Then
                cboTemplate.SelectedItem = "Lube Email Transmittal.txt"
            Else
                cboTemplate.SelectedItem = "Fuels Email Transmittal.txt"
            End If
        Catch ex As System.Exception
            Err.WriteLog("SecureSend-GetTemplates", ex)

        End Try
    End Sub


    Private Function FillTemplate(template As String) As String

        Dim fileContents As String = Nothing
        Dim DoubleCheck As String = ""

        Dim SiteName As String = ""

        Dim today As Date

        Try
            Dim params = New List(Of String)
            ' Consultant Nant
            params = New List(Of String)
            params.Add("Initials/" + mUser)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    ConsultantName = ds.Tables(0).Rows(0).Item("email").ToString()
                End If
            End If

            params.Add("RefNum/" + RefNum)

            If mStudy <> "RAM" Then
                ds = db.ExecuteStoredProc("Console.GetTSortData", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Coloc = ds.Tables(0).Rows(0)("Coloc").ToString().Trim
                    End If
                End If
            Else
                ds = db.ExecuteStoredProc("Console.GetCompanyContactInfo", params)
                If ds.Tables.Count > 0 Then
                    For Each Row In ds.Tables(0).Rows
                        If Not FirstName.Contains(Row("First Name").ToString().Trim & " " & Row("Last Name").ToString().Trim) Then FirstName += Row("First Name").ToString().Trim & " " & Row("Last Name").ToString().Trim + ", "
                    Next
                End If



                ds = db.ExecuteStoredProc("Console.GetSiteInfo", params)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        SiteName = ds.Tables(0).Rows(0)("SiteName").ToString().Trim
                    End If
                End If
            End If

            If txtResponseDays.Text.Length > 0 Then
                today = Utilities.ValFaxDateDue(txtResponseDays.Text)
            End If


            If template <> "" Then

                fileContents = My.Computer.FileSystem.ReadAllText(TemplatePath & template)

                fileContents = fileContents.Replace("~Site~", SiteName)
                fileContents = fileContents.Replace("~RefName~", Coloc)
                fileContents = fileContents.Replace("~DueDate~", today.ToString("dd-MMM-yy"))
                fileContents = fileContents.Replace("~UserID~", ConsultantName)
                fileContents = fileContents.Replace("~FirstName~", FirstName)


            End If



        Catch ex As System.Exception
            Err.WriteLog("SecureSend-FillTemplate", ex)

        End Try
        Return fileContents


    End Function
    Private Sub CopyToTempDir()

        Dim f As String
        Dim start As Integer
        Utilities.DeleteFiles(TempPath)
        Try
            For i = 0 To lstFilesToSend.Items.Count - 1
                start = lstFilesToSend.Items(i).ToString.LastIndexOf("\") + 1
                f = lstFilesToSend.Items(i).ToString.Substring(start, lstFilesToSend.Items(i).ToString.Length - start)
                File.Copy(lstFilesToSend.Items(i), mTempPath & f)
            Next
        Catch
        End Try

    End Sub
    Private Function GetExtension(strFile As String)
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, 3)
        Catch
            ext = ""
        End Try
        Return ext
    End Function
    Private Sub SendEmail()

        Dim Zip As Boolean = False

        Dim success As Boolean = True
        Dim answer As DialogResult
        Dim mfile As String
        Dim fileExt As String
        Dim i As Integer = 0

        Dim ext As String

        If txtEmail.Text = "" Then
            MessageBox.Show("Must Enter a Email Address")
            Exit Sub
        End If

        For i = 0 To lstFilesToSend.Items.Count - 1
            If GetExtension(lstFilesToSend.Items(i).ToString()).ToUpper() <> "DOC" And GetExtension(lstFilesToSend.Items(i).ToString()).ToUpper() <> "XLS" Then
                Zip = True
            End If
        Next
        If Zip Then
            Dim rtn As DialogResult = MessageBox.Show("It is Solomon's policy to not send unencrypted file via email.  You are attempting to send a file that cannot be encrypted.  Do you wish to send an encrypted ZIP file instead?", "Wait", MessageBoxButtons.YesNo)
            If rtn = System.Windows.Forms.DialogResult.Yes Then cboSendMethod.SelectedIndex = 1
        End If
        Select Case cboSendMethod.Text.Substring(0, 3)
            Case "PPF"

                Try
                    subj = My.Computer.FileSystem.OpenTextFileReader(TemplatePath & "\" & cboTemplate.Text).ReadLine
                Catch
                End Try


                Try
                    For i = 0 To lstFilesToSend.Items.Count - 1


                        mfile = lstFilesToSend.Items(i).ToString
                        ext = mfile.Substring(mfile.LastIndexOf(".") + 1, mfile.Length - mfile.LastIndexOf(".") - 1)
                        fileExt = ext.ToUpper.Substring(0, 3)

                        If txtPassword.Text.Trim.Length = 0 And (fileExt = "XLS" Or fileExt = "DOC") Then
                            MessageBox.Show("Company password is missing.  Must include a password for Excel or Word documents with PPF Send Method")
                            txtPassword.ReadOnly = False
                            txtPassword.Enabled = True
                            txtPassword.Focus()
                            Exit Sub
                        Else
                            txtPassword.ReadOnly = True

                            Select Case fileExt
                                Case "XLS"
                                    success = Utilities.ExcelPassword(mfile, txtPassword.Text.Trim)
                                Case "DOC", "OCX"
                                    If Not mfile.ToUpper.Contains("INSTRUCTIONS") Then
                                        success = Utilities.WordPassword(mfile, txtPassword.Text.Trim)
                                    Else
                                        success = True
                                    End If

                            End Select

                            If Not success Then
                                MessageBox.Show("An Error has occurred in the packaging of the file: " & mfile & ".  Please make sure all files exist and are of the correct type.")
                                Exit Sub
                            End If
                        End If
                    Next



                    ''Attach all the files in the TempDir


                    Dim attachments As New List(Of String)

                    For i = 0 To lstFilesToSend.Items.Count - 1
                        attachments.Add(lstFilesToSend.Items(i).ToString)
                    Next
                    If subj.Length = 0 Then subj = Study & " Study Correspondence for " & RefNum
                    If lstFilesToSend.Items.Count > 0 Then SendOtherEmail("Notification of Transmittal - " & subj, "", True, "Notification of Transmittal.txt")
                    SendOtherEmail(subj, "", True, cboTemplate.Text, attachments)

                Catch ex As System.Exception
                    Err.WriteLog("SecureSend Load", ex)

                    MessageBox.Show(ex.Message, "Send Email")
                End Try

            Case "DLM"
                Dim body As String
                'Attach all the files in the TempDir
                Dim zipfile As String = ZipFiles()
                body = "Files link: " & DLM_Link(zipfile)
                If subj.Length = 0 Then subj = Study & " Study Correspondence for " & RefNum
                SendOtherEmail(subj, body, True, cboTemplate.Text)

            Case "PPZ"
                Dim zipfile As List(Of String) = New List(Of String)
                Dim nZip As String = ZipFiles()
                If nZip.Length = 0 Then Exit Select
                zipfile.Add(nZip)

                If subj.Length = 0 Then subj = Study & " Study Correspondence for " & RefNum
                If lstFilesToSend.Items.Count > 0 Then SendOtherEmail("Notification of Transmittal - " & subj, "", True, "Notification of Transmittal.txt")
                SendOtherEmail(subj, "", True, cboTemplate.Text, zipfile)

        End Select
        CloseSS()


    End Sub
    Private Sub CloseSS()
        Clear()
        Me.Close()

    End Sub
    Private Function BuildIDREmail(mfile As String) As String
        Dim TemplateValues As New WordTemplate()
        Dim IDRConsultantName As String


        TemplateValues.Field.Add("_TodaysDate")
        TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))


        Dim Row As DataRow
        Dim params As List(Of String)
        ' Company Name
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)

        If ds.Tables.Count > 0 Then
            Row = ds.Tables(0).Rows(0)

            TemplateValues.Field.Add("_Company")
            TemplateValues.RField.Add(Row("Company").ToString)


            TemplateValues.Field.Add("_Refinery")
            TemplateValues.RField.Add(Row("Location").ToString)
            Coloc = Row("Coloc").ToString

        End If




        ' Contact Name
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
        If ds.Tables.Count = 0 Then
            strFaxName = "NAME UNAVAILABLE"
            strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
        Else
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)
                strFaxName = Row("CoordName").ToString
                strFaxEmail = Row("CoordEmail").ToString
            End If
        End If



        TemplateValues.Field.Add("_Contact")
        TemplateValues.RField.Add(strFaxName)
        ' Contact Email Address

        TemplateValues.Field.Add("_EMail")
        TemplateValues.RField.Add(strFaxEmail)

        ' Consultant Nant
        params = New List(Of String)
        params.Add("Initials/" + mUser)
        ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)

                If Row("ConsultantName") Is Nothing Or IsDBNull(Row("ConsultantName")) Then
                    IDRConsultantName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
                Else
                    IDRConsultantName = Row("ConsultantName").ToString
                End If
            Else

                IDRConsultantName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
            End If
        End If


        TemplateValues.Field.Add("_ConsultantName")
        TemplateValues.RField.Add(IDRConsultantName)
        ' Consultant Initials

        TemplateValues.Field.Add("_Initials")

        TemplateValues.RField.Add(mUser)
        Utilities.WordTemplateReplace(TemplatePath & mfile, TemplateValues, TempPath & mfile, 0)
        Return TempPath & mfile
    End Function

    Private Sub SendOtherEmail(subject As String, nbody As String, display As Boolean, Optional template As String = Nothing, Optional attaches As List(Of String) = Nothing)


        Dim body As String = nbody
        Try
            body = "Dear " & CompanyContactName & "," & vbCrLf & vbCrLf
            body += FillTemplate(template).Replace(subj, "")
            body += vbCrLf + vbCrLf + ConsultantName

            subject = subject.Replace("~RefName~", Coloc)


            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    attaches(i) = attaches(i).ToString
                Next
            End If

            If Not subject.Contains("Notification of Transmittal") Then
                Utilities.SendEmail(txtEmail.Text, txtCC.Text + IIf(chkPCC.Checked, txtPCC.Text, ""), subject, body, attaches, display)
            Else
                Utilities.SendEmail(txtEmail.Text, "", subject, body, attaches, display)
            End If

            If attaches IsNot Nothing Then
                For i = 0 To attaches.Count - 1
                    File.Delete(attaches(i).ToString)
                Next
            End If

            If chkSaveCopy.Checked And Not subject.Contains("Notification of Transmittal") Then
                Dim strw As New StreamWriter(CorrPath & Date.Now.ToString("yyyy-MM-dd hh.mm") & " (SENT) - " & subject & ".txt")
                strw.WriteLine(body)
                strw.Close()
                strw.Dispose()
            End If

        Catch ex As System.Exception
            Err.WriteLog("SecureSend-SendOtherEmail", ex)
            MessageBox.Show(ex.Message, "Send Other Email")
            CloseSS()
        End Try
    End Sub


    Public Function DLM_Link(file As String) As String
        ' Download Manager
        Dim servClt As New ServiceClient
        ' CALL STATEMENT
        Return servClt.GetDownloadLink(txtEmail.Text, txtPassword.Text, file, 1, mRefNum, 0, mUser, mRefNum)

    End Function
    Public Sub RecurseTree(ByVal Node As TreeNodeCollection, path As String)

        For Each tn As TreeNode In Node
            If tn.Checked And tn.GetNodeCount(False) = 0 Then
                lstFoundNode.Add(tn.Tag)
            End If

            RecurseTree(tn.Nodes, path)

        Next


    End Sub
    Private Function TraverseTree(ByVal treeView As TreeView, path As String) As List(Of String)

        lstFoundNode = New List(Of String)
        Dim nodeStack As TreeNodeCollection = treeView.Nodes
        RecurseTree(nodeStack, path)

        Return lstFoundNode
    End Function

    Private Sub btnSend_Click(sender As System.Object, e As System.EventArgs) Handles btnSend.Click
        Try
            Me.Cursor = Cursors.WaitCursor

            If txtPassword.Text.Length > 0 Then Clipboard.SetText(txtPassword.Text)

            Dim p As Process = Utilities.CheckProcess("C:\Program Files (x86)\Microsoft Office\Office14\", "Outlook")
            SendEmail()
            Me.Cursor = Cursors.Default
        Catch ex As System.Exception
            Err.WriteLog("SecureSend-btnSend_Click", ex)
            MessageBox.Show(ex.Message, "btnSend_Click")
            CloseSS()

        End Try
    End Sub

    Private Function ZipFiles() As String

        If Not Directory.Exists(TempPath & "ZIP\") Then
            Directory.CreateDirectory(TempPath & "ZIP\")
        End If
        Dim FN As String = Directory.GetFiles(TempPath)(0).ToString
        FN = FN.Substring(FN.LastIndexOf("\") + 1, FN.Length - FN.LastIndexOf("\") - 1)
        FN = FN.Replace(GetExtension(FN), "ZIP")


        Dim zipFile As String = mTempPath & "ZIP\" & FN
        Dim newFile = InputBox("Because you have a file that is not a WORD or EXCEL document, you must send your email with an encrypted zip file. " & vbCrLf & "Enter Zip filename: ", "Enter Filename", FN)
        If newFile.Length = 0 Then
            Return newFile
        End If
        newFile = TempPath & "ZIP\" & newFile
        Utilities.Zipfile(TempPath, txtPassword.Text, newFile)
        Return newFile

    End Function


    Private Sub lstFilesToSend_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles lstFilesToSend.DragDrop
        Dim dataObject As OutlookDataObject = New OutlookDataObject(e.Data)


        'get the names and data streams of the files dropped
        Dim filenames() As String = CType(dataObject.GetData("FileGroupDescriptorW"), String())
        Dim ans As String
        ans = InputBox(CorrPath & " : ", "Save File to:", filenames(0).Trim)
        lstFilesToSend.Items.Add(CorrPath & "\" & filenames(0).Trim)

        Dim filestreams() As MemoryStream = CType(dataObject.GetData("FileContents"), MemoryStream())

        For fileIndex = 0 To filenames.Length - 1

            'use the fileindex to get the name and data stream
            Dim filename As String = filenames(fileIndex)
            Dim filestream As MemoryStream = filestreams(fileIndex)

            'save the file stream using its name to the application path
            Dim outputStream As FileStream = File.Create(CorrPath & "\" & ans)
            filestream.WriteTo(outputStream)
            outputStream.Close()

        Next

    End Sub

    Private Sub lstFilesToSend_DragEnter(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles lstFilesToSend.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub lstFilesToSend_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles lstFilesToSend.KeyDown
        If e.KeyCode = Keys.Delete Then
            File.Delete(lstFilesToSend.SelectedItem)
            lstFilesToSend.Items.Remove(lstFilesToSend.SelectedItem)

        End If
    End Sub


    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub btnAddFile_Click(sender As System.Object, e As System.EventArgs)

        fileDialog.Title = "Please Select a File"
        fileDialog.InitialDirectory = "K:\STUDY\" & mStudy & "\"
        fileDialog.ShowDialog()

    End Sub

    Private Sub fdlg_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles fileDialog.FileOk

        Dim file As String = fileDialog.FileName
        Dim start As Integer = file.LastIndexOf("\") + 1
        Dim len As Integer = file.Length
        Dim fileLength As Integer = len - start
        lstFilesToSend.Items.Add(file)
        FileCopy(file, TempPath & file.Substring(start, fileLength))

    End Sub

    Private Sub chkPCC_CheckedChanged(sender As Object, e As EventArgs) Handles chkPCC.CheckedChanged
        If chkPCC.Checked Then
            txtPCC.Enabled = True
            txtPCC.Text = PCCEmail
        Else
            txtPCC.Enabled = False
            txtPCC.Text = ""
        End If

    End Sub
End Class
