﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Me.txtLogin = New System.Windows.Forms.TextBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.lblError = New System.Windows.Forms.Label()
        Me.REFINING = New System.Windows.Forms.RadioButton()
        Me.OLEFINS = New System.Windows.Forms.RadioButton()
        Me.RAM = New System.Windows.Forms.RadioButton()
        Me.POWER = New System.Windows.Forms.RadioButton()
        Me.TPL = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'txtLogin
        '
        Me.txtLogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLogin.Location = New System.Drawing.Point(117, 12)
        Me.txtLogin.Name = "txtLogin"
        Me.txtLogin.Size = New System.Drawing.Size(155, 32)
        Me.txtLogin.TabIndex = 0
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(117, 57)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(155, 32)
        Me.txtPassword.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Login:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 24)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Password:"
        '
        'btnLogin
        '
        Me.btnLogin.Location = New System.Drawing.Point(101, 110)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(75, 23)
        Me.btnLogin.TabIndex = 2
        Me.btnLogin.Text = "Login"
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.ForeColor = System.Drawing.Color.Red
        Me.lblError.Location = New System.Drawing.Point(115, 94)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 13)
        Me.lblError.TabIndex = 4
        Me.lblError.Visible = False
        '
        'REFINING
        '
        Me.REFINING.AutoSize = True
        Me.REFINING.Location = New System.Drawing.Point(37, 149)
        Me.REFINING.Name = "REFINING"
        Me.REFINING.Size = New System.Drawing.Size(76, 17)
        Me.REFINING.TabIndex = 5
        Me.REFINING.TabStop = True
        Me.REFINING.Text = "REFINING"
        Me.REFINING.UseVisualStyleBackColor = True
        '
        'OLEFINS
        '
        Me.OLEFINS.AutoSize = True
        Me.OLEFINS.Location = New System.Drawing.Point(172, 149)
        Me.OLEFINS.Name = "OLEFINS"
        Me.OLEFINS.Size = New System.Drawing.Size(70, 17)
        Me.OLEFINS.TabIndex = 6
        Me.OLEFINS.TabStop = True
        Me.OLEFINS.Text = "OLEFINS"
        Me.OLEFINS.UseVisualStyleBackColor = True
        '
        'RAM
        '
        Me.RAM.AutoSize = True
        Me.RAM.Location = New System.Drawing.Point(37, 172)
        Me.RAM.Name = "RAM"
        Me.RAM.Size = New System.Drawing.Size(49, 17)
        Me.RAM.TabIndex = 7
        Me.RAM.TabStop = True
        Me.RAM.Text = "RAM"
        Me.RAM.UseVisualStyleBackColor = True
        '
        'POWER
        '
        Me.POWER.AutoSize = True
        Me.POWER.Location = New System.Drawing.Point(172, 172)
        Me.POWER.Name = "POWER"
        Me.POWER.Size = New System.Drawing.Size(66, 17)
        Me.POWER.TabIndex = 8
        Me.POWER.TabStop = True
        Me.POWER.Text = "POWER"
        Me.POWER.UseVisualStyleBackColor = True
        '
        'TPL
        '
        Me.TPL.AutoSize = True
        Me.TPL.Location = New System.Drawing.Point(37, 195)
        Me.TPL.Name = "TPL"
        Me.TPL.Size = New System.Drawing.Size(57, 17)
        Me.TPL.TabIndex = 9
        Me.TPL.TabStop = True
        Me.TPL.Text = "PL && T"
        Me.TPL.UseVisualStyleBackColor = True
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(286, 143)
        Me.Controls.Add(Me.TPL)
        Me.Controls.Add(Me.POWER)
        Me.Controls.Add(Me.RAM)
        Me.Controls.Add(Me.OLEFINS)
        Me.Controls.Add(Me.REFINING)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtLogin)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Login"
        Me.Text = "Console"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtLogin As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents REFINING As System.Windows.Forms.RadioButton
    Friend WithEvents OLEFINS As System.Windows.Forms.RadioButton
    Friend WithEvents RAM As System.Windows.Forms.RadioButton
    Friend WithEvents POWER As System.Windows.Forms.RadioButton
    Friend WithEvents TPL As System.Windows.Forms.RadioButton
End Class
