﻿Imports System.Data.SqlClient
Imports SA.Internal.Console.DataObject

Public Class Login
    Dim db As DataObject
    Dim RefNum As String
    Dim UserName As String
    Dim Password As String
    Dim StudyType As String
    Dim StudyYear As String
    Dim Entered As Boolean = False
    Dim Console As Object
    Dim VPN As Boolean = False
    Dim ScreenName As String = ""

    Private Sub LoginClick()
        Try
            lblError.Visible = False

            If RAM.Checked Then StudyType = "RAM"
            If OLEFINS.Checked Then StudyType = "OLEFINS"
            If POWER.Checked Then StudyType = "POWER"
            If REFINING.Checked Then StudyType = "REFINING"
            If TPL.Checked Then StudyType = "PIPELINES"
            Dim tpw As String = txtLogin.Text.ToUpper

            Select Case StudyType.ToUpper
                Case "REFININGDEV"

                    db = New DataObject(DataObject.StudyTypes.REFININGDev, txtLogin.Text, txtPassword.Text)

                    Console = New MainConsole()
                Case "REFINING"

                    db = New DataObject(DataObject.StudyTypes.REFINING, txtLogin.Text, txtPassword.Text)

                    Console = New MainConsole()

                Case "OLEFINS"
                    db = New DataObject(DataObject.StudyTypes.OLEFINS, txtLogin.Text, txtPassword.Text)
                    Console = New OlefinsMainConsole()
                Case "POWER"
                    db = New DataObject(DataObject.StudyTypes.POWER, txtLogin.Text, txtPassword.Text)
                    Console = New PowerMainConsole()
                Case "RAM"
                    'CHANGE
                    'db = New DataObject(DataObject.StudyTypes.RAM, "mgv", "solomon2012")
                    If CheckRAMPassword(txtLogin.Text, txtPassword.Text) Then
                        UserName = ScreenName
                        Console = New RAMMainConsole()
                    Else
                        lblError.Text = "Invalid Login"
                        lblError.Visible = True
                        Exit Sub
                    End If
                Case "PIPELINES"
                    db = New DataObject(DataObject.StudyTypes.PIPELINES, txtLogin.Text, txtPassword.Text)
                    Console = New TPMainConsole()

            End Select

            If db.DBError IsNot Nothing Then
                lblError.Text = "Invalid Login"
                lblError.Visible = True
                Exit Sub
            End If


            Console.StudyType = StudyType
            If RefNum <> "" Then
                Console.ReferenceNum = RefNum
                Console.Spawn = True
            End If

            Console.Password = txtPassword.Text
            Console.UserName = IIf(UserName Is Nothing, txtLogin.Text, UserName)
            Console.CurrentStudyYear = My.Settings.Item("CurrentStudyYear")
            Console.VPN = VPN

            Console.Show()
            Me.Hide()
            Me.WindowState = FormWindowState.Minimized
            If txtLogin.Text <> "DBB" And txtLogin.Text <> "JDW" And txtLogin.Text <> "EJB" Then Me.Close()
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
            lblError.Text = "Invalid Login"
            lblError.Visible = True

        End Try


    End Sub

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click

        Entered = True
        Me.Cursor = Cursors.WaitCursor
        LoginClick()
        Me.Cursor = Cursors.Default
    End Sub

    Private Function CheckRAMPassword(RAMUserName As String, RAMPassword As String) As Boolean
        
        Dim strPassword As String = ""
        Dim CompanyID As String = "000SAI"
        Dim pw As String = ""

        Dim params As New List(Of String)
        params.add("UserID/" & RAMUserName)


        Dim ds As DataSet = db.ExecuteStoredProc("Console.GetRAMPassword", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                strPassword = ds.Tables(0).Rows(0)("Password")
                ScreenName = ds.Tables(0).Rows(0)("ScreenName")
                CompanyID = ds.Tables(0).Rows(0)("CompanyID")
            End If
            Dim salt As String = "dog" + ScreenName.ToLower + "butt" & CompanyID.Trim & "steelers"
            pw = Utilities.EncryptedPassword(txtPassword.Text, salt)
        End If

        If pw = strPassword Then
            Return True
        Else
            Return False
        End If

    End Function
    

    Private Sub Login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        If My.Settings.Testing Then
            Me.Text = " **Testing **"
            Me.Height = 260
        Else
            Me.Height = 180
        End If

        VPN = False
        Dim s() As String = System.Environment.GetCommandLineArgs()
        If s.Length > 1 Then
            If s(1).Length > 2 Then
                StudyType = s(1)
                StudyYear = s(2)
                If s.Length > 3 Then VPN = s(3)
            End If
        Else
            StudyType = "REFINING"
            StudyYear = "14"
        End If

        Me.Text = Me.Text + StudyType & " 20" & StudyYear
        'Dim rd As RadioButton = Controls.Find(StudyType, False)(0)
        'rd.Checked = True
        Me.Show()
        If UserName = "" Then
            If StudyType = "RAM" Then
                txtLogin.Text = Environment.UserName & "@sa"
            Else
                txtLogin.Text = Environment.UserName
            End If
        End If

        If txtLogin.Text <> "" Then txtPassword.Focus()



        Me.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) / 2
        Me.Top = (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) / 2
    End Sub

    Private Sub txtPassword_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress, txtLogin.KeyPress
        If e.KeyChar = Chr(13) Then LoginClick()
    End Sub

End Class
